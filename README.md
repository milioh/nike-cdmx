## Synopsis

App Híbrida para comunicación interna de Nike CDMX.

## Code Example

La página web esta en:

* [nikecdmx.wachinango.com](http://nikecdmx.wachinango.com)

## Motivation

Se requiere mejorar la organización y planeación de toda la información relevante con la oficina de Nike CDMX para mejorar la experiencia de las visitas de los equipos internacionales.

## Installation

La carpeta del proyecto es "www", solo hay que instalar el contenido en un web server.

## Contributors

* Emiliano Hernández ([@milioh](https://twitter.com/milioh))

## License

Copyright 2015 - WACHINANGO

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.