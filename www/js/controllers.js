angular.module('nikecdmx.controllers', [])

.controller('StartCtrl', function($scope, $state, $location, $window) {

	//Open Register View
	$scope.register = function(event) {
		$state.go('register');
	}

	//Open Login View
	$scope.login = function(event) {
		$state.go('login');
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Redirect To Home App
			$state.go('thecity');
		}
	});

})

.controller('LoginCtrl', function($scope, $state, $location, $window, $ionicPopup, $timeout, API) {

	function convertoBase64Image(img) {
		// Create an empty canvas element
		var canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;

		// Copy the image contents to the canvas
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		// Get the data-URL formatted image
		// Firefox supports PNG and JPEG. You could check img.src to guess the
		// original format, but be aware the using "image/jpg" will re-encode the image.
		var dataURL = canvas.toDataURL("image/png");
			return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
	}

	//Validate Login
	$scope.submit = function (event) {

		//Read Values
		var email = $('#inputEmailLogin').val().trim().toLowerCase();
		var password = $('#inputPasswordLogin').val().trim();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		//Check Values
		if (email != '' && password != '')
		{
			//Check Valid Email
            if (regex.test(email))
            {
	            //Call Parse Do Login
	            API.doLogin(email,password)
            	.success(function(data) {
	            	//Set Session
            		$window.localStorage.setItem("nikecdmx_session", JSON.stringify(data));

            		//Save Avatar image to Local Storage
            		// http://stackoverflow.com/questions/31903405/parse-com-and-fabric-js-how-to-fix-cors-error-for-hosted-images

            		//Set Current User
            		Parse.User.become(data.sessionToken).then(function (user) {
					  // The current user is now set to user.
					}, function (error) {
					  // The token could not be validated.
					});

					//Redirect To State
					$state.go(data.state);
				})
				.error(function(data) {
					console.log(data);
					//Show Alert Email Error
					var alertPopup = $ionicPopup.alert({
						title: 'NIKE CDMX',
						template: 'Wrong Credentials. Try again.',
						okType: 'button-assertive'
					});
					alertPopup.then(function(res) {
				     	console.log('Email not valid.');
					})
				});
	        }
	        else
	        {
		        //Show Alert Email Error
				var alertPopup = $ionicPopup.alert({
					title: 'NIKE CDMX',
					template: 'You have to input a valid email address.',
					okType: 'button-assertive'
				});
				alertPopup.then(function(res) {
			     	console.log('Email not valid.');
				});
	        }
		}
		else
		{
			//Show Alert No Data
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: 'You should input your Nike Email Address & Password.',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('No data input.');
			});
		}
	}

	//Open Forgot View
	$scope.forgot = function(event) {
		$state.go('forgot');
	}

	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Redirect To Home App
			$state.go('thecity');
		}

		//Reset Form
		$('#inputEmail').val('');
		$('#inputPassword').val('');
	});
})

.controller('ForgotCtrl', function($scope, $state, $location, $window, $ionicPopup, API) {

	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	}

	//Reset Password
	$scope.submit = function (event)
	{
		//Read Values
		var email = $('#inputEmailForgot').val().trim();
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		//Check Values
		if (email)
		{
			//Check Valid Email
            if (regex.test(email))
            {
	            //Destroy Sessions
	            Parse.User.logOut();

		        //Request Password Reset
	            Parse.User.requestPasswordReset(email, {
					success: function() {
						//Show Alert Email Error
						var alertPopup = $ionicPopup.alert({
							title: 'NIKE CDMX',
							template: 'Reset Password Instructions send to this email.',
							okType: 'button-assertive'
						});
						alertPopup.then(function(res) {
					     	console.log('Reset Password Instructions Send.');
					     	$window.history.back();
						});
					},
					error: function(error) {
						console.log(error);
					    //Show Alert Parse Error
						var alertPopup = $ionicPopup.alert({
							title: 'NIKE CDMX',
							template: '<center>' + error.message + '</center>',
							okType: 'button-assertive'
						});
						alertPopup.then(function(res) {
					     	console.log('Email not valid.');
						});
					}
				});
	        }
	        else
	        {
		        //Show Alert Email Error
				var alertPopup = $ionicPopup.alert({
					title: 'NIKE CDMX',
					template: 'You have to input a valid email address.',
					okType: 'button-assertive'
				});
				alertPopup.then(function(res) {
			     	console.log('Email not valid.');
				});
	        }
	    }
	    else
	    {
		    //Show Alert No Data
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: 'You should input your Nike Email Address & Password.',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('No data input.');
			});
	    }
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {
		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Redirect To Home App
			$state.go('thecity');

			//Reset Form
			$('#inputEmailForgot').val('');
		}
	});
})

.controller('RegisterCtrl', function($scope, $state, $location, $window, $cordovaCamera, $ionicActionSheet, $ionicPopup, $timeout, $http, API) {

	//Image Data
	var imageData;

	// Set the default value of inputType
	$scope.inputType = 'password';

	//Open Disclaimer View
	$scope.disclaimer = function(event) {

		//Read Values
		var username = $('#inputUsernameRegister').val().trim();
		var email = $('#inputEmailRegister').val().trim().toLowerCase();
		var password = $('#inputPasswordRegister').val().trim();
		var gender = $('#inputGenderRegister').val().trim();
		var country = $('#inputCountryRegister').val().trim();
		var src = $('#file').attr("src");
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var valid = false;
		var teamID = "";

		//Check Image
		if (src != 'img/photo.png')
		{
			//Check Values
			if (username && email && password)
			{
				//Check Valid Email
	            if (regex.test(email))
	            {
		            //Check if email is a valid from Parse Team
		            $.each($scope.team, function() {
			            var emailTeam = this.email.trim().toLowerCase();
			            if (emailTeam === email) { valid = true; teamID = this.objectId; }
			        });

					//Check Email valid
					if (valid)
					{
						//Destroy Sessions
			            Parse.User.logOut();

			            //Upload Image
			            var image_name = new Date().getTime();
			            var dataToSubmit = {__ContentType : "image/jpeg", base64 : imageData};
			            var parseFile = new Parse.File(image_name+'.jpg', dataToSubmit);
			            parseFile.save();

			            //Call Parse Create User
			            var user = new Parse.User();
						user.set("username", email);
						user.set("password", password);
						user.set("email", email);

						// other fields can be set just like with Parse.Object
						user.set("nickname", username);
						user.set("country", country);
						user.set("state", "disclaimer");
						user.set("gender", gender);
						user.set("avatar", parseFile);

						user.signUp(null, {
							success: function(user) {
								// Hooray! Let them use the app now.
								console.log(user);

								//Call Parse Team Update
					            var Team = Parse.Object.extend("team");
								var query = new Parse.Query(Team);
								query.get(teamID, {
									success: function(objTeam) {
										objTeam.set("installed", true);

										objTeam.save(null, {
										  	success: function(team) {
										    	console.log(team)
										  	}
										});
								    },
									error: function(object, error) {
								    }
								});

								//Call Parse Do Login
					            API.doLogin(email,password)
				            	.success(function(data) {
					            	//Set Session
				            		$window.localStorage.setItem("nikecdmx_session", JSON.stringify(data));

									//Set Current User
				            		Parse.User.become(data.sessionToken).then(function (user) {
									  // The current user is now set to user.
									}, function (error) {
									  // The token could not be validated.
									});

									//Redirect To Home App
									$state.go('disclaimer');
								})
								.error(function(data) {
									console.log(data);
									//Show Alert Email Error
									var alertPopup = $ionicPopup.alert({
										title: 'NIKE CDMX',
										template: 'Server connection error. Try again later.',
										okType: 'button-assertive'
									});
									alertPopup.then(function(res) {
								     	console.log('Email not valid.');
									})
								});
							},
							error: function(user, error) {
					   			// Show the error message somewhere and let the user try again.
					   			console.log("Error: " + error.code + " " + error.message);
					   			//Show Alert Create USer
								var alertPopup = $ionicPopup.alert({
									title: 'NIKE CDMX',
									template: error.message,
									okType: 'button-assertive'
								});
								alertPopup.then(function(res) {
							     	console.log('Email not valid.');
								});
					   		}
					   	});
					}
					else
					{
						//Show Alert Create USer
						var alertPopup = $ionicPopup.alert({
							title: 'NIKE CDMX',
							template: "<center>Your Email is not valid to use this app. Contact an Administrator.</center>",
							okType: 'button-assertive'
						});
						alertPopup.then(function(res) {
					     	console.log('Email is not within at Team List.');
						});
					}
		        }
		        else
		        {
			        //Show Alert Email Error
					var alertPopup = $ionicPopup.alert({
						title: 'NIKE CDMX',
						template: 'You have to input a valid email address.',
						okType: 'button-assertive'
					});
					alertPopup.then(function(res) {
				     	console.log('Email not valid.');
					});
		        }
			}
			else
			{
				//Show Alert No Data
				var alertPopup = $ionicPopup.alert({
					title: 'NIKE CDMX',
					template: 'You should input data in all fields.',
					okType: 'button-assertive'
				});
				alertPopup.then(function(res) {
			     	console.log('No data input.');
				});
			}
		}
		else
		{
			//Show Alert No Data
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: '<center>You have to select a photo<br/>from camera or album.</center>',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('No data input.');
			});
		}
	}

	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	}

	//Open Camara / Photos
	$scope.upload = function (event) {
		//Check Version
		var isIOS = ionic.Platform.isIOS();
		if (isIOS)
		{
			// Show the action sheet
			var hideSheet = $ionicActionSheet.show({
		     	buttons: [
			 		{ text: 'Camera' },
			 		{ text: 'Show Album' }
			 	],
			 	titleText: 'Select Photo Source',
			 	cancelText: 'Cancel',
			 	cancel: function() {
		        	// add cancel code..
		        },
				buttonClicked: function(index) {
					if (index == 0)
					{
						var options = {
				            quality: 75,
				            destinationType: Camera.DestinationType.DATA_URL,
				            sourceType: Camera.PictureSourceType.CAMERA,
				            allowEdit: true,
				            encodingType: Camera.EncodingType.JPEG,
				            targetWidth: 300,
				            targetHeight: 300,
				            popoverOptions: CameraPopoverOptions,
				            saveToPhotoAlbum: false
				        };

				        $cordovaCamera.getPicture(options).then(function (returnedImageData) {
					        imageData = returnedImageData;
					        var image = new Image();
							image.src = "data:image/jpeg;base64," + imageData;
							$('.image').html('<img src="' + image.src + '">');
				        }, function (err) {
				            // An error occured. Show a message to the user
				            console.log(err);
				        });
					}
					else
					{
						var options = {
				        	quality: 75,
							destinationType: Camera.DestinationType.DATA_URL,
							sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
							allowEdit: true,
							encodingType: Camera.EncodingType.JPEG,
							targetWidth: 300,
							targetHeight: 300,
							popoverOptions: CameraPopoverOptions,
							saveToPhotoAlbum: false
						};

				        $cordovaCamera.getPicture(options).then(function (returnedImageData) {
					         imageData = returnedImageData;
				            var image = new Image();
							image.src = "data:image/jpeg;base64," + imageData;
							$('.image').html('<img src="' + image.src + '">');
				        }, function (err) {
				            // An error occured. Show a message to the user
				        });
					}
					return true;
				},
				cssClass: ''
		   });
		}
		else
		{
			//Show Alert Browser
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: 'This function only works on iPhone.',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('Device not iPhone.');
			});
		}
	}

	//Show Password
	$scope.showPassword = function(){
    	if ($scope.inputType == 'password')
			$scope.inputType = 'text';
		else
			$scope.inputType = 'password';
	};

	//Load View
	$scope.$on('$ionicView.enter', function() {
		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Redirect To Home App
			$state.go('thecity');
		}

		//Default Image
		$('.image').html('<img src="img/photo.png" id="file">');

		// Set the default value of inputType
		$scope.inputType = 'password';

		//Read Valid Team
		API.getTeam()
			.success(function(data) {
				$scope.team = data.results;
				console.log($scope.team);
			})
			.error(function(data) {
				console.log(data);
				//Show Alert No Connection
				var alertPopup = $ionicPopup.alert({
					title: 'NIKE CDMX',
					template: 'Server connection error. Try again later.',
					okType: 'button-assertive'
				});
				alertPopup.then(function(res) {
			     	console.log('No Team List retrieved.');
				});
			});
	});
})

.controller('DisclaimerCtrl', function($scope, $state, $location, $window) {

	//Back Navigation
	$scope.backNav = function (event) {
		$window.history.back();
	}

	//Validate Register
	$scope.submit = function (event) {

		//Update State
		var User = Parse.User.current();
		User.set("state","thecity");
		User.save();

		//Redirect The City
		$state.go('thecity');
	}

})

/* THE CITY */
.controller('TheCityCtrl', function($scope, $state, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $ionicSlideBoxDelegate, $sce, TheCity, CDMX, History, Landmarks, $cordovaAppVersion, $ionicPopup, $timeout) {

	$rootScope.versionParse = "0.0.0";
  	$rootScope.appVersion = "0.0.0";
  	$rootScope.downloadPage = "https://wachinango.com/nike/cdmxapp/";
  	$rootScope.alertUpdate = false;
  	$rootScope.agenda = "img/data/agenda/program.png";

	//Read All Cities
	$scope.cities_results = TheCity.all();

	//Read All CDMX Numbers
	$scope.cdmx_results = CDMX.all();

	//Read All History
	$scope.history_results = History.all();

	//Read All Landmarks
	$scope.landmarks_results = Landmarks.all();

	//Open Menu
	$rootScope.toggleLeft = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};

	//Show City
	$scope.showCity = function(event,view) {
		$state.go(view);
	}

	//Open The City View
	$rootScope.goTheCity = function(event) {
	  	$state.go('thecity');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open The Brand View
  	$rootScope.goTheBrand = function(event) {
	  	$state.go('thebrand');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open The Costumer View
  	$rootScope.goTheConsumer = function(event) {
	  	$state.go('theconsumer');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open The Marketplace
  	$rootScope.goTheMarketplace = function(event) {
	  	$state.go('themarketplace');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open Agenda
  	$rootScope.goAgenda = function(event) {
	  	$state.go('agenda');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open The Tour of Today
  	$rootScope.goTheTourOfToday = function(event) {
	  	$state.go('thetouroftoday');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open Chingopedia
  	$rootScope.goChingopedia = function(event) {
	  	$state.go('chingopedia');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open Settings
  	$rootScope.goSettings = function(event) {
	  	$state.go('settings');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open Photofeed User
  	$rootScope.goPhotofeed = function(event) {
	  	$state.go('photofeed');
	  	$ionicSideMenuDelegate.toggleLeft();
  	}

  	//Open Photofeed User
  	$rootScope.goPhotofeedUser = function(event) {
	  	$state.go('photofeeduser');
  	}

  	//Open Photofeed Create
  	$rootScope.goPhotofeedCreate = function(event) {
	  	$state.go('photofeedcreate');
  	}

  	//Open Logout
  	$rootScope.goLogout = function(event) {
	  	$state.go('logout');
  	}

  	//Back Navigation
  	$rootScope.backNav = function (event) {
		$window.history.back();
	}

  	//Load View
  	$scope.$on('$ionicView.enter', function() {
  		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			Parse.initialize("yIkjB91C7MHUk4lynlMpOraUnsebIZWjCqiF1XxW", "6IkckRHiZl3qIu08OrNplEubWGvIhq0w1nRHxzDi");

			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
			//console.log($rootScope.session);
		  	//Nickname
		  	$rootScope.nickname = $rootScope.session.nickname;
		  	$rootScope.avatar = $rootScope.session.avatar.url;

		  	//Version Check
			document.addEventListener("deviceready", function () {

				if($window.Connection) {
	                if(navigator.connection.type == Connection.WIFI) {
	                    $cordovaAppVersion.getVersionNumber().then(function (version) {
							//Read App Version
						  	Parse.Config.get().then(function(config) {
							  	if(!$rootScope.$$phase) {
								  	$rootScope.$apply(function() {
									  	$rootScope.versionParse = config.get("version");
										$rootScope.downloadPage = config.get("downloadPage");
								  	});
								};
								//alert("alert ParseConfig:" + $rootScope.versionParse);
							}, function(error) {
								console.log("Error Config");
								console.log(error);
							});

					    	$rootScope.appVersion = version;

					    	$timeout(function() {
							    //alert("alert deviceReady versionParse:" + $rootScope.versionParse);
						    	//alert("alert deviceReady appVersion:" + $rootScope.appVersion);
						    	console.log($rootScope.appVersion);

						    	//Check Versions
						    	if ($rootScope.versionParse != $rootScope.appVersion)
						    	{
							    	//alert("alertUpdate: " + $rootScope.alertUpdate);
							    	if (!$rootScope.alertUpdate)
							    	{
								    	if(!$rootScope.$$phase) {
									    	$rootScope.$apply(function() {
										    	$rootScope.alertUpdate = true;
										    });
										}
								    	var confirmPopup = $ionicPopup.confirm({
											title: 'Nike CDMX',
											template: '<center>There is a new version of "Nike CDMX City Guide", would you like to go to the download page?</center>'
										});
										confirmPopup.then(function(res) {
											if(res) {
												$window.open($rootScope.downloadPage, '_system', 'location=yes');
											} else {
												console.log('Not updated.');
											}
										});
									}
								}
							}, 1000);
					    });
	                }
	            }
			}, false);
		}
  	})

  	//Show Landmark
  	$scope.showLandmark = function ($event,id)
  	{
	  	$state.go('landmarkdetail', { landmarkId: id });
  	}

  	//Show Video
  	$rootScope.showVideo = function ($event)
  	{
	  	//Play Video
	  	$("#hispanic")[0].load();
	  	$('#hispanic').get(0).play();
  	}
})

/* LANDMARK DETAIL */
.controller('LandmarkDetailCtrl', function($scope, $state, $stateParams, $location, $window, $rootScope, Landmarks) {

	//Read Landmark Info
	$scope.landmark = Landmarks.get($stateParams.landmarkId);

})

/* THE BRAND */
.controller('TheBrandCtrl', function($scope, $state, $location, $window, $rootScope, $ionicSlideBoxDelegate, TheBrand) {

	//Read All Cities
	$scope.graphs_results = TheBrand.all();

})

/* THE CONSUMER */
.controller('TheConsumerCtrl', function($scope, $state, $location, $window, $rootScope, TheConsumer) {

	//Read All Costumers
	$scope.consumer_results = TheConsumer.all();

	//Show Consumer View
	$scope.showConsumer = function(event,id)
	{
		$state.go('theconsumerdetail', { consumerId: id });
	}

})

.controller('TheConsumerDetailCtrl', function($scope, $state, $stateParams, $location, $window, $rootScope, TheConsumer) {

	//Read Costumer Info
	$scope.consumer = TheConsumer.get($stateParams.consumerId);

})

/* THE MARKETPLACE */
.controller('TheMarketplaceCtrl', function($scope, $state, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $ionicSlideBoxDelegate, $sce, TheMarketplace, TheCityCenter, South, East, West, North, NorthEast, OurLeadingRetailConcepts, TheMustSeeDoors) {

	//Read All Marketplaces
	$scope.marketplaces_results = TheMarketplace.all();

	//Read All Consumpton Map
	$scope.thecitycenter_results = TheCityCenter.all();
	$scope.south_results = South.all();
	$scope.east_results = East.all();
	$scope.west_results = West.all();
	$scope.north_results = North.all();
	$scope.northeast_results = NorthEast.all();

	//Read All Retailers
	$scope.retailers_results = OurLeadingRetailConcepts.all();

	//Read All Must See Doors
	$scope.stores_results = TheMustSeeDoors.all();

	//CONSUMPTON MAP
	$scope.showTheCityCenter = function(event) { $state.go('thecitycenter'); }
	$scope.showSouth = function(event) { $state.go('south'); }
	$scope.showEast = function(event) { $state.go('east'); }
	$scope.showWest = function(event) { $state.go('west'); }
	$scope.showNorth = function(event) { $state.go('north'); }
	$scope.showNorthEast = function(event) { $state.go('northeast'); }

	//Show Marketplace View
	$scope.showMarketplace = function(event,view)
	{
		$state.go(view);
	}

	//Set Default
	$scope.word = 0;

	//Show Words
	$scope.showData = function (event, id)
	{
		if ($scope.word == 0)
		{
			$scope.word = id;
		}
		else
		{
			$scope.word = 0;
		}
	}
})

/* AGENDA */
.controller('AgendaCtrl', function($scope, $state, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $sce, Agenda, $ionicLoading, $timeout) {

	//Read All Events
	//$scope.agenda_results = Agenda.all();

	//Data Agenda
	$rootScope.agenda = 'img/data/agenda/program.png';
	$rootScope.lastUpdate = new Date(2015, 11, 14);
	$scope.showLastUpdate = false;

	//Show Agenda Detail View
	$scope.showEvent = function(event,id)
	{
		$state.go('agendadetail', { eventId: id });
	}

	//Show Create Event View
	$scope.createEvent = function(event)
	{
		$state.go('agendacreate');
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Read App Version
	  	/*Parse.Config.get().then(function(config) {

		  	// Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
			$rootScope.agenda = config.get("agenda");
			$rootScope.lastUpdate = config.get("agendaLastUpdate");
			console.log($rootScope.agenda);
			console.log($rootScope.lastUpdate);

			//alert("alert ParseConfig:" + $rootScope.versionParse);
		}, function(error) {
			console.log("Error Config");
			console.log(error);
			//Data Agenda
			$rootScope.agenda = 'img/data/agenda/program.png';
			$rootScope.lastUpdate = new Date(2015, 11, 14);
			console.log($rootScope.agenda);
			console.log($rootScope.lastUpdate);
		});

		// Setup the loader
		$ionicLoading.show({
		    content: 'Loading',
		    animation: 'fade-in',
		    showBackdrop: true,
		    maxWidth: 200,
		    showDelay: 0
		});

		$timeout(function () {
		    $ionicLoading.hide();
		    $rootScope.$apply(function() {
			    $scope.showLastUpdate = true;
		    });
		}, 2000);*/

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
		}

		var results = [];

		if (!$scope.agendas)
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});

			var objAgenda;
			var agendasQuery = Parse.Object.extend("agenda");
			var query = new Parse.Query(agendasQuery);
			query.ascending("createdAt");
			query.find({
				success: function(dataAgenda) {
					for (var i = 0; i < dataAgenda.length; i++) {
						var objectAgenda = dataAgenda[i];
						objAgenda = {
					        'createdAt': objectAgenda.createdAt,
					        'id': objectAgenda.id,
					        'title': objectAgenda.get("title"),
					        'detail': objectAgenda.get("detail")
				        }
				        results.push(objAgenda);
				    }
				    $scope.agendas = results;
				},
				error: function(error) {
					alert("Error: " + error.code + " " + error.message);
				}
			});

			// Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
			$timeout(function () {
		    	$ionicLoading.hide();
				$scope.$apply(function() {
					$scope.agendas = results;
					console.log($scope.agendas);
				});
			}, 2000);
		}
	});

	//Show Agenda
	$scope.showAgenda = function (event,id)
	{
		$state.go('agendadetail', { eventId: id });
	}
})

.controller('AgendaDetailCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $ionicSlideBoxDelegate, $sce, $ionicLoading, $ionicPopup, $timeout) {

	//Read Event
	$scope.agenda;

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
		}

		var results = [];


		// Setup the loader
		$ionicLoading.show({
	    	content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var objAgenda;
		var agendaQuery = Parse.Object.extend("agenda_event");
		var query = new Parse.Query(agendaQuery);
		query.equalTo("objectId", $stateParams.eventId);
		query.find({
			success: function(dataAgenda) {
				for (var i = 0; i < dataAgenda.length; i++) {
					var objectAgenda = dataAgenda[i];
					objAgenda = {
				        'createdAt': objectAgenda.createdAt,
				        'id': objectAgenda.id,
				        'title': 'EVENT DETAIL',
				        'image': objectAgenda.get("image"),
				        'update': objectAgenda.get("update")
			        }
			        $scope.agenda = objAgenda;
			    }
			},
			error: function(error) {
				alert("Error: " + error.code + " " + error.message);
			}
		});

		// Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
		$timeout(function () {
	    	$ionicLoading.hide();
			$scope.$apply(function() {
				$scope.agenda = objAgenda;
				console.log($scope.agenda);
			});
		}, 2000);
	});
})

.controller('AgendaCreateCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $sce, Agenda) {

	//Show Member Modal
	$scope.showMembers = function (event)
	{
		alert('show modal members');
	}

	//Show Demo Create Event
	$scope.showDemo = function (event,id)
	{
		$state.go('agendadetail', { eventId: id });
	}
})

/* THE TOUR OF TODAY */
.controller('TheTourOfTodayCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $sce, $ionicLoading, $ionicPopup, $timeout, TheTourOfToday, API) {

	//Read All Events
	$scope.courses_results = TheTourOfToday.all();

	//Show Store
	$scope.showDates = function (event,id)
	{
		$state.go('thedatesoftoday', { dateId: id });
		/*
		if (id != 7)
		{
			$state.go('thecourseoftoday', { courseId: id });
		}
		else
		{
			$state.go('houseofher', { courseId: id });
		}
		*/
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
		}

		var results = [];

		if (!$scope.results)
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});

			var results = [];
			var results_dates = [];
			var tourQuery = Parse.Object.extend("tour_event");
			var query = new Parse.Query(tourQuery);
			query.equalTo("status", true);
			query.find({
				success: function(data) {
					data.sort(function(a, b){
					    return a.createdAt - b.createdAt;
					});
					for (var i = 0; i < data.length; i++) {
						var object = data[i];
						var dates_array = object.get("dates");
						var obj = {
					        'createdAt': object.createdAt,
					        'id': object.id,
					        'title': object.get("title"),
					        'dates': dates_array
				        }
				        results.push(obj);
				    }
			   	},
			   	error: function(error) {
			    	alert("Error: " + error.code + " " + error.message);
				}
			});

			// Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
			$timeout(function () {
		    	$ionicLoading.hide();
				$scope.$apply(function() {
					$scope.results = results;
				});
			}, 2000);
		}
	});

})

.controller('TheDateOfTodayCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $sce, $ionicLoading, $ionicPopup, $timeout, TheTourOfToday, API) {

	//Read All Events
	//$scope.courses_results = TheTourOfToday.all();

	//Show Store
	$scope.showStore = function (event,id)
	{
		$state.go('thecourseoftoday', { courseId: id });
		/*
		if (id != 7)
		{
			$state.go('thecourseoftoday', { courseId: id });
		}
		else
		{
			$state.go('houseofher', { courseId: id });
		}
		*/
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
		}

		var results = [];
		var dateId = $stateParams.dateId;

		if (!$scope.results)
		{
			// Setup the loader
			$ionicLoading.show({
		    	content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});

			var results = [];
			var results_dates = [];
			var tourQuery = Parse.Object.extend("tour");
			var query = new Parse.Query(tourQuery);
			var dates_array = [];
			var stores_array_ordered = [];
			query.equalTo("objectId", dateId);
			query.find({
				success: function(data) {
					for (var i = 0; i < data.length; i++) {
						var object = data[i];
						dates_array = object.get("dates");
						for (var j = 0; j < dates_array.length; j++) {
							var tourDateQuery = Parse.Object.extend("tour_date");
							var queryTourDate = new Parse.Query(tourDateQuery);
							queryTourDate.equalTo("objectId", dates_array[j]);
							queryTourDate.find({
								success: function(dataDates) {
									var results_stores = [];
									for (var k = 0; k < dataDates.length; k++) {
										var objectDate = dataDates[k];
										var stores_array = objectDate.get("stores");
										var objStore = {
									        'id': objectDate.id,
									        'stores': stores_array,
								        }
										stores_array_ordered.push(objStore);
										if (stores_array)
										{
											for (var l = 0; l < stores_array.length; l++) {
												var tourStoresQuery = Parse.Object.extend("tour_store");
												var queryTourStores = new Parse.Query(tourStoresQuery);
												queryTourStores.equalTo("objectId", stores_array[l]);
												queryTourStores.find({
													success: function(dataStore) {
														for (var m = 0; m < dataStore.length; m++) {
															var objectStore = dataStore[m];
															var objStore = {
														        'createdAt': objectStore.createdAt,
														        'id': objectStore.id,
														        'title': objectStore.get("title"),
														        'cover': objectStore.get("cover"),
														        'header': objectStore.get("header"),
														        'chart': objectStore.get("chart"),
													        }
													    }
													    results_stores.push(objStore);
													},
													error: function(error) {
														alert("Error: " + error.code + " " + error.message);
													}
												});
											}
									    }
									    var objDate = {
									        'createdAt': objectDate.createdAt,
									        'id': objectDate.id,
									        'title': objectDate.get("title"),
									        'stores': results_stores
								        }
									}
									results_dates.push(objDate);
								},
							   	error: function(error) {
							    	alert("Error: " + error.code + " " + error.message);
								}
							});
						}

						var obj = {
					        'createdAt': object.createdAt,
					        'id': object.id,
					        'title': object.get("title"),
					        'dates': results_dates
				        }
				    }
			        results.push(obj);
			   	},
			   	error: function(error) {
			    	alert("Error: " + error.code + " " + error.message);
				}
			});

			// Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
			$timeout(function () {
		    	$ionicLoading.hide();
		    	var objetos = []; var objetos_ordered = []; last_objet = [];
		    	var tiendas = []; var tiendas_ordered = [];
				$scope.$apply(function() {

					//Sort Dates
					$.each(results, function () {
					   $.each(this.dates, function () {
						   	$.each(this.stores, function () {
							   	tiendas.push(this);
							});
							objetos.push(this);
					   });
					});
					for (var i = 0; i < dates_array.length; i++)
					{
					    for (var j = 0; j < objetos.length; j++)
					    {
						    if (dates_array[i] == objetos[j].id)
						    {
							    var nuevo = [];
							    for (var k = 0; k < stores_array_ordered.length; k++)
							    {
								    if (dates_array[i] == stores_array_ordered[k].id)
								    {
								    	//Leemos las Tiendas
								    	for (var l = 0; l < stores_array_ordered[k].stores.length; l++)
								    	{
									    	//Leemos las Tiendas
									    	for (var m = 0; m < tiendas.length; m++)
									    	{
										    	if (stores_array_ordered[k].stores[l] == tiendas[m].id)
										    	{
											    	console.log(stores_array_ordered[k].stores[l]);
											    	console.log(tiendas[m]);
											    	nuevo.push(tiendas[m]);
										    	}
										    }
									    	//console.log(stores_array_ordered[k].stores[l]);
									    	//objetos[j].stores_ordered = push(stores_array_ordered[k].stores[l]);
								    	}
								    }
							    }
							    objetos[j].stores_ordered = nuevo;
							    objetos_ordered.push(objetos[j]);
						    }
						}
					}
					results[0].dates_ordered = objetos_ordered;
					console.log(stores_array_ordered);
					console.log(tiendas);
					console.log(results);
					$scope.results = results;
				});
			}, 3000);
		}
	});

})

.controller('TheCourseOfTodayCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $ionicSlideBoxDelegate, $sce, TheCourseOfToday, $ionicLoading, $ionicPopup, $timeout) {

	//Read Course Data
	//$scope.course = TheCourseOfToday.get($stateParams.courseId);

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
		}

		var results = [];

		// Setup the loader
		$ionicLoading.show({
	    	content: 'Loading',
			animation: 'fade-in',
			showBackdrop: true,
			maxWidth: 200,
			showDelay: 0
		});

		var objStore;
		var tourStoresQuery = Parse.Object.extend("tour_store");
		var query = new Parse.Query(tourStoresQuery);
		query.equalTo("objectId", $stateParams.courseId);
		query.find({
			success: function(dataStore) {
				for (var i = 0; i < dataStore.length; i++) {
					var objectStore = dataStore[i];
					objStore = {
				        'createdAt': objectStore.createdAt,
				        'id': objectStore.id,
				        'title': objectStore.get("title"),
				        'cover': objectStore.get("cover"),
				        'header': objectStore.get("header"),
				        'chart': objectStore.get("chart"),
			        }
			        $scope.store = objStore;
			    }
			},
			error: function(error) {
				alert("Error: " + error.code + " " + error.message);
			}
		});

		// Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
		$timeout(function () {
	    	$ionicLoading.hide();
			$scope.$apply(function() {
				$scope.store = objStore;
				console.log($scope.store);
			});
		}, 2000);
	});
})

.controller('HouseofHerCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $ionicSlideBoxDelegate, $sce, TheCourseOfToday) {

	//Read Course Data
	$scope.course = TheCourseOfToday.get(7);

	$scope.videoHOH = function ($event)
	{
		//Play Video
	  	$("#hoh")[0].load();
	  	$('#hoh').get(0).play();
	}
})

/* CHINGOPEDIA */
.controller('ChingopediaCtrl', function($scope, $state, $stateParams, $location, $window, $ionicSideMenuDelegate, $ionicScrollDelegate, $rootScope, $sce, Chingopedia) {

	//Read All Events
	$scope.words_results = Chingopedia.all();

	//Set Default
	$scope.word = 0;

	//Show Words
	$scope.showWords = function (event, id)
	{
		if ($scope.word == 0)
		{
			$scope.word = id;
		}
		else
		{
			$scope.word = 0;
		}
	}
})

/* SETTINGS */
.controller('SettingsCtrl', function($scope, $state, $stateParams, $location, $window, $cordovaCamera, $ionicActionSheet, $ionicPopup, $rootScope, $timeout, $http, API) {

	//Image Data
	var imageData;

	//Open Disclaimer View
	$scope.saveChanges = function(event) {

		//Read Values
		var username = $('#inputUsernameSettings').val().trim();
		var gender = $('#inputGenderSettings').val().trim();
		var country = $('#inputCountrySettings').val().trim();
		var src = $('#fileSettings').attr("src");
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		//Check Values
		if (username)
		{
			//Check if Image Change
			if (src != $rootScope.session.avatar.url)
			{
	            //Upload Image
	            var image_name = new Date().getTime();
	            var dataToSubmit = {__ContentType : "image/jpeg", base64 : imageData};
	            var parseFile = new Parse.File(image_name+'.jpg', dataToSubmit);
	            parseFile.save();
	        }

            //Call Parse Create User
            var objUser = Parse.User.current();
			objUser.set("nickname", username);
			objUser.set("country", country);
			objUser.set("gender", gender);
			//Update Avatar
			if (src != $rootScope.session.avatar.url)
			{
				objUser.set("avatar", parseFile);
			}

			objUser.save(null, {
				success: function(user) {
					// Hooray! Let them use the app now.
					console.log(user);
					//Update Data in Session
					var session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
					session.nickname = user.get('nickname');
				    session.gender = user.get('gender');
				    session.country = user.get('country');
				    session.avatar = user.get("avatar");
				    $window.localStorage.setItem("nikecdmx_session", JSON.stringify(session));
				    //Update Root Scope
				    $rootScope.$apply(function() {
					    $rootScope.nickname = session.nickname;
					    $rootScope.avatar = session.avatar.url;
					});
					$('.image').html('<img src="' + user.get("avatar").url() +'">');
				    //Show Alert Create USer
					var alertPopup = $ionicPopup.alert({
						title: 'NIKE CDMX',
						template: "<center>User update successfully</center>",
						okType: 'button-assertive'
					});
					alertPopup.then(function(res) {
				     	console.log('Update Successfully');
					});
				},
				error: function(user, error) {
		   			// Show the error message somewhere and let the user try again.
		   			console.log("Error: " + error.code + " " + error.message);
		   			//Show Alert Create USer
					var alertPopup = $ionicPopup.alert({
						title: 'NIKE CDMX',
						template: error.message,
						okType: 'button-assertive'
					});
					alertPopup.then(function(res) {
				     	console.log('Update Error');
					});
		   		}
		   	});
		}
		else
		{
			//Show Alert No Data
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: 'You should input data in all fields.',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('No data input.');
			});
		}
	}

	//Open Camara / Photos
	$scope.upload = function (event) {
		//Check Version
		var isIOS = ionic.Platform.isIOS();
		if (isIOS)
		{
			// Show the action sheet
			var hideSheet = $ionicActionSheet.show({
		     	buttons: [
			 		{ text: 'Camera' },
			 		{ text: 'Show Album' }
			 	],
			 	titleText: 'Select Photo Source',
			 	cancelText: 'Cancel',
			 	cancel: function() {
		        	// add cancel code..
		        },
				buttonClicked: function(index) {
					if (index == 0)
					{
						var options = {
				            quality: 75,
				            destinationType: Camera.DestinationType.DATA_URL,
				            sourceType: Camera.PictureSourceType.CAMERA,
				            allowEdit: true,
				            encodingType: Camera.EncodingType.JPEG,
				            targetWidth: 300,
				            targetHeight: 300,
				            popoverOptions: CameraPopoverOptions,
				            saveToPhotoAlbum: false
				        };

				        $cordovaCamera.getPicture(options).then(function (returnedImageData) {
					        imageData = returnedImageData;
					        var image = new Image();
							image.src = "data:image/jpeg;base64," + imageData;
							$('.image_update').html('<img src="' + image.src + '" id="fileSettings">');
				        }, function (err) {
				            // An error occured. Show a message to the user
				            console.log(err);
				        });
					}
					else
					{
						var options = {
				        	quality: 75,
							destinationType: Camera.DestinationType.DATA_URL,
							sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
							allowEdit: true,
							encodingType: Camera.EncodingType.JPEG,
							targetWidth: 300,
							targetHeight: 300,
							popoverOptions: CameraPopoverOptions,
							saveToPhotoAlbum: false
						};

				        $cordovaCamera.getPicture(options).then(function (returnedImageData) {
					         imageData = returnedImageData;
				            var image = new Image();
							image.src = "data:image/jpeg;base64," + imageData;
							$('.image_update').html('<img src="' + image.src + '" id="fileSettings">');
				        }, function (err) {
				            // An error occured. Show a message to the user
				        });
					}
					return true;
				},
				cssClass: ''
		   });
		}
		else
		{
			//Show Alert Browser
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: 'This function only works on iPhone.',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('Device not iPhone.');
			});
		}
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
			//console.log($rootScope.session);
		}

		//Default Image
		$('.image_update').html('<img src="' + $rootScope.session.avatar.url +'" id="fileSettings">');

		// Gender
		$scope.gender = $rootScope.session.gender;

		// Country
		$scope.country = $rootScope.session.country;
	});
})

/* PHOTOFEED USER */
.controller('PhotofeedUserCtrl', function($scope, $state, $stateParams, $location, $window, $cordovaCamera, $ionicActionSheet, $ionicLoading, $ionicPopup, $rootScope, $timeout, $http, API) {

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
			console.log($rootScope.session);
		}

		var id = $rootScope.session.objectId;
		var results = [];

		// Setup the loader
	  $ionicLoading.show({
	    content: 'Loading',
	    animation: 'fade-in',
	    showBackdrop: true,
	    maxWidth: 200,
	    showDelay: 0
	  });

	  // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
	  $timeout(function () {
	    $ionicLoading.hide();
	    $scope.$apply(function() {
			$scope.results = results;

			var count = Object.keys(results).length;
			if (!count)
			{
				var confirmPopup = $ionicPopup.confirm({
					title: 'Nike CDMX',
					template: '<center>There isn’t any photo with your user.<br/>Would you like upload the first one?</center>'
				});
				confirmPopup.then(function(res) {
					if(res) {
						$state.go('photofeedcreate');
					} else {
						console.log('Not updated.');
					}
				});
			}
		});
	  }, 2000);

		API.getPhotofeedUser(id).success(function(data){
	        var contador;
	        console.log(data);
	        //Proccess Results
	        $.each(data.results, function() {
		        contador++;
		        var object = this;
		        var ownerID = this.owner.objectId;
		        var owner = [];
		        var userQuery = Parse.Object.extend("_User");
				var query = new Parse.Query(userQuery);
				query.equalTo("objectId", ownerID);
				query.find({
					success: function(objOwner) {
						var obj = {
					        'owner': objOwner[0].get("nickname"),
					        'avatar': objOwner[0].get("avatar").url(),
					        'createdAt': object.createdAt,
					        'image': object.image.url,
					        'description': object.description
				        }
				        results.push(obj);
						console.log(results);
				   	},
				   	error: function(error) {
				    	alert("Error: " + error.code + " " + error.message);
					}
				});
				console.log(object);
			});
	    });
	    $scope.results = results;
	});
})

/* PHOTOFEED CREATE */
.controller('PhotofeedCreateCtrl', function($scope, $state, $stateParams, $location, $window, $cordovaCamera, $ionicActionSheet, $ionicPopup, $rootScope, $timeout, $http, API) {

	//Image Data
	var imageData;

	$scope.date = new Date();

	//Open Disclaimer View
	$scope.publish = function(event) {

		//Read Values
		var userId = $rootScope.session.objectId;
		var description = $('#description').val().trim();
		var src = $('#fileUpload').attr("src");

		//Check if Image Change
		if (src != "img/placeholder_camera.png")
		{
            //Upload Image
            var image_name = new Date().getTime();
            var dataToSubmit = {__ContentType : "image/jpeg", base64 : imageData};
            var parseFile = new Parse.File(image_name+'.jpg', dataToSubmit);
            parseFile.save();

			//Read User
			var objUser = Parse.User.current();

	        //Call Parse Create Object
	        var photo = Parse.Object.extend("photofeed");
			var photofeed = new photo();
			photofeed.set("description", description);
			photofeed.set("image", parseFile);
			photofeed.set("owner", objUser);

			photofeed.save(null, {
				success: function(photofeed) {
					// Hooray! Let them use the app now.
					console.log(photofeed);
					//Show Alert Create USer
					var alertPopup = $ionicPopup.alert({
						title: 'NIKE CDMX',
						template: "<center>Publish photo successfully</center>",
						okType: 'button-assertive'
					});
					alertPopup.then(function(res) {
				     	console.log('Publish Successfully');
					});
					$('#new').html('<img class="full-image" src="img/placeholder_camera.png" id="fileUpload">');
					$state.go("photofeed");
				},
				error: function(user, error) {
		   			// Show the error message somewhere and let the user try again.
		   			console.log("Error: " + error.code + " " + error.message);
		   			//Show Alert Create USer
					var alertPopup = $ionicPopup.alert({
						title: 'NIKE CDMX',
						template: error.message,
						okType: 'button-assertive'
					});
					alertPopup.then(function(res) {
				     	console.log('Update Error');
					});
		   		}
		   	});
		}
		else
		{
			//Show Alert Create USer
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: "You have to choose a photo",
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('Update Error');
			});
		}
	}

	//Open Camara / Photos
	$scope.upload = function (event) {
		//Check Version
		var isIOS = ionic.Platform.isIOS();
		if (isIOS)
		{
			// Show the action sheet
			var hideSheet = $ionicActionSheet.show({
		     	buttons: [
			 		{ text: 'Camera' },
			 		{ text: 'Show Album' }
			 	],
			 	titleText: 'Select Photo Source',
			 	cancelText: 'Cancel',
			 	cancel: function() {
		        	// add cancel code..
		        },
				buttonClicked: function(index) {
					if (index == 0)
					{
						var options = {
				            quality: 75,
				            destinationType: Camera.DestinationType.DATA_URL,
				            sourceType: Camera.PictureSourceType.CAMERA,
				            allowEdit: true,
				            encodingType: Camera.EncodingType.JPEG,
				            targetWidth: 640,
				            targetHeight: 640,
				            popoverOptions: CameraPopoverOptions,
				            saveToPhotoAlbum: false
				        };

				        $cordovaCamera.getPicture(options).then(function (returnedImageData) {
					        imageData = returnedImageData;
					        var image = new Image();
							image.src = "data:image/jpeg;base64," + imageData;
							$('#new').html('<img src="' + image.src + '" id="fileUpload" class="full-image">');
				        }, function (err) {
				            // An error occured. Show a message to the user
				            console.log(err);
				        });
					}
					else
					{
						var options = {
				        	quality: 75,
							destinationType: Camera.DestinationType.DATA_URL,
							sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
							allowEdit: true,
							encodingType: Camera.EncodingType.JPEG,
							targetWidth: 640,
							targetHeight: 640,
							popoverOptions: CameraPopoverOptions,
							saveToPhotoAlbum: false
						};

				        $cordovaCamera.getPicture(options).then(function (returnedImageData) {
					         imageData = returnedImageData;
				            var image = new Image();
							image.src = "data:image/jpeg;base64," + imageData;
							$('#new').html('<img src="' + image.src + '" id="fileUpload" class="full-image">');
				        }, function (err) {
				            // An error occured. Show a message to the user
				        });
					}
					return true;
				},
				cssClass: ''
		   });
		}
		else
		{
			//Show Alert Browser
			var alertPopup = $ionicPopup.alert({
				title: 'NIKE CDMX',
				template: 'This function only works on iPhone.',
				okType: 'button-assertive'
			});
			alertPopup.then(function(res) {
		     	console.log('Device not iPhone.');
			});
		}
	}

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
			//console.log($rootScope.session);
		}

		//Default Image
		$('#new').html('<img class="full-image" src="img/placeholder_camera.png" id="fileUpload">');

		$('#description').val('');
	});

})

/* PHOTOFEED */
.controller('PhotofeedCtrl', function($scope, $state, $stateParams, $location, $window, $cordovaCamera, $ionicLoading, $ionicActionSheet, $ionicPopup, $rootScope, $timeout, $http, API) {

	//Image Data
	var imageData;

	//Open Photofeed User
  	$rootScope.goPhotofeedIntern = function(event) {
	  	$state.go('photofeed');
  	}

	//Load View
	$scope.$on('$ionicView.enter', function() {

		//Check Session
  		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			$rootScope.session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));
			//console.log($rootScope.session);
		}

		var results = [];

		// Setup the loader
	  $ionicLoading.show({
	    content: 'Loading',
	    animation: 'fade-in',
	    showBackdrop: true,
	    maxWidth: 200,
	    showDelay: 0
	  });

	  // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
	  $timeout(function () {
	    $ionicLoading.hide();
	    $scope.$apply(function() {
			$scope.results = results;
			var count = Object.keys(results).length;
			if (!count)
			{
				var confirmPopup = $ionicPopup.confirm({
					title: 'Nike CDMX',
					template: '<center>There isn\'t any photo yet.<br/>Would you like upload the first one?</center>'
				});
				confirmPopup.then(function(res) {
					if(res) {
						$state.go('photofeedcreate');
					} else {
						console.log('Not updated.');
					}
				});
			}
		});
	  }, 2000);

		API.getPhotofeedAll().success(function(data){
	        var contador;
	        //Proccess Results
	        $.each(data.results, function() {
		        var object = this;
		        var ownerID = this.owner.objectId;
		        var owner = [];
		        var userQuery = Parse.Object.extend("_User");
				var query = new Parse.Query(userQuery);
				query.equalTo("objectId", ownerID);
				query.find({
					success: function(objOwner) {
						var obj = {
					        'owner': objOwner[0].get("nickname"),
					        'avatar': objOwner[0].get("avatar").url(),
					        'createdAt': object.createdAt,
					        'image': object.image.url,
					        'description': object.description
				        }
				        results.push(obj);
				   	},
				   	error: function(error) {
				    	alert("Error: " + error.code + " " + error.message);
					}
				});
			});
	    });
	    $scope.results = results;
	});
})

/* LOGOUT */
.controller('LogoutCtrl', function($scope, $state, $stateParams, $location, $window, $rootScope, API) {

	//Load View
  	$scope.$on('$ionicView.enter', function() {
		//Check Session
		if ($window.localStorage.getItem("nikecdmx_session"))
		{
			//Read Values
			var session = JSON.parse($window.localStorage.getItem("nikecdmx_session"));

			//LogOut
			API.doLogout(session.sessionToken).success(function(data){});
		}

		//Destroy Session
		$window.localStorage.removeItem("nikecdmx_session");

		//Redirect Start
		$state.go('start');
	})
})