// Ionic NikeCDMX App
angular.module('nikecdmx', ['ionic','ngCordova','nikecdmx.controllers','nikecdmx.services'])

.run(function($ionicPlatform, $cordovaStatusbar) {
	$ionicPlatform.ready(function() {
    	// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if(window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    	}
		if(window.StatusBar) {
			$cordovaStatusbar.styleColor('black');
    	}
    	
    	Parse.initialize("yIkjB91C7MHUk4lynlMpOraUnsebIZWjCqiF1XxW", "6IkckRHiZl3qIu08OrNplEubWGvIhq0w1nRHxzDi");
    	
    	if (window.cordova) {
			cordova.getAppVersion(function(version) {
				appVersion = version;
			});
		}
  	});
})

.filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }
})

.filter('timeago',function($sce){
    var s = 1000;
    var m = s * 60;
    var h = m * 60;
    return function(date) {
        var now = Date.now();
        var d = date.getTime() - now;
        console.log(d);
        if (d > h)
            return '' + d / h + ' hours ago';
        if (d > m)
            return '' + d / m + ' minutes ago';
        if (d > s)
            return '' + d / s + ' seconds ago';
        return '' + d + ' ms ago';
    }
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $compileProvider) {

	$stateProvider
	
	.state('start', {
    	url: '/start',
		templateUrl: 'templates/start.html',
        controller: 'StartCtrl'
  	})
  	
  	.state('register', {
    	url: '/register',
		templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'
  	})
  	
  	.state('disclaimer', {
    	url: '/disclaimer',
		templateUrl: 'templates/disclaimer.html',
        controller: 'DisclaimerCtrl'
  	})
  	
  	.state('login', {
    	url: '/login',
		templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
  	})
  	
  	.state('forgot', {
    	url: '/forgot',
		templateUrl: 'templates/forgot.html',
        controller: 'ForgotCtrl'
  	})
  	
  	/* THE CITY */
  	.state('thecity', {
    	url: '/thecity',
		templateUrl: 'templates/thecity/index.html',
        controller: 'TheCityCtrl'
  	})
  	
  	.state('cdmx', {
    	url: '/cdmx',
		templateUrl: 'templates/thecity/cdmx.html',
        controller: 'TheCityCtrl'
  	})
  	
  	.state('history', {
    	url: '/history',
		templateUrl: 'templates/thecity/history.html',
        controller: 'TheCityCtrl'
  	})
  	
  	.state('landmarks', {
    	url: '/landmarks',
		templateUrl: 'templates/thecity/landmarks.html',
        controller: 'TheCityCtrl'
  	})
  	
  	.state('landmarkdetail', {
      	url: '/landmarks/:landmarkId',
      	templateUrl: 'templates/thecity/landmark-detail.html',
        controller: 'LandmarkDetailCtrl'
    })
  	
  	/* THE BRAND */
  	.state('thebrand', {
    	url: '/thebrand',
		templateUrl: 'templates/thebrand/index.html',
        controller: 'TheBrandCtrl'
  	})
  	
  	/* THE CONSUMER */
  	.state('theconsumer', {
    	url: '/theconsumer',
		templateUrl: 'templates/theconsumer/index.html',
        controller: 'TheConsumerCtrl'
  	})
  	
  	.state('theconsumerdetail', {
      	url: '/theconsumer/:consumerId',
      	templateUrl: 'templates/theconsumer/detail.html',
        controller: 'TheConsumerDetailCtrl'
    })
    
    /* THE MARKETPLACE */
  	.state('themarketplace', {
    	url: '/themarketplace',
		templateUrl: 'templates/themarketplace/index.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('thecitycenter', {
    	url: '/thecitycenter',
		templateUrl: 'templates/themarketplace/consumptonmap/thecitycenter.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('south', {
    	url: '/south',
		templateUrl: 'templates/themarketplace/consumptonmap/south.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('east', {
    	url: '/east',
		templateUrl: 'templates/themarketplace/consumptonmap/east.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('west', {
    	url: '/west',
		templateUrl: 'templates/themarketplace/consumptonmap/west.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('north', {
    	url: '/north',
		templateUrl: 'templates/themarketplace/consumptonmap/north.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('northeast', {
    	url: '/northeast',
		templateUrl: 'templates/themarketplace/consumptonmap/northeast.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('ourleadingretail', {
    	url: '/ourleadingretail',
		templateUrl: 'templates/themarketplace/ourleadingretail.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	.state('themustseedoors', {
    	url: '/themustseedoors',
		templateUrl: 'templates/themarketplace/themustseedoors.html',
        controller: 'TheMarketplaceCtrl'
  	})
  	
  	/* AGENDA */
  	.state('agenda', {
    	url: '/agenda',
		templateUrl: 'templates/agenda/index.html',
        controller: 'AgendaCtrl'
  	})
  	
  	.state('agendadetail', {
      	url: '/agenda/event/:eventId',
      	templateUrl: 'templates/agenda/detail.html',
        controller: 'AgendaDetailCtrl'
    })
    
    .state('agendacreate', {
      	url: '/agenda/create',
      	templateUrl: 'templates/agenda/create.html',
        controller: 'AgendaCreateCtrl'
    })
    
    /* THE TOUR OF TODAY */
  	.state('thetouroftoday', {
    	url: '/thetouroftoday',
		templateUrl: 'templates/thetouroftoday/index.html',
        controller: 'TheTourOfTodayCtrl'
  	})
  	
  	.state('thedatesoftoday', {
      	url: '/thetouroftoday/date/:dateId',
      	templateUrl: 'templates/thetouroftoday/date.html',
        controller: 'TheDateOfTodayCtrl'
    })
  	
  	.state('thecourseoftoday', {
      	url: '/thetouroftoday/course/:courseId',
      	templateUrl: 'templates/thetouroftoday/course.html',
        controller: 'TheCourseOfTodayCtrl'
    })
    
    .state('houseofher', {
      	url: '/thetouroftoday/houseofher',
      	templateUrl: 'templates/thetouroftoday/houseofher.html',
        controller: 'HouseofHerCtrl'
    })
    
    /* CHINGOPEDIA */
  	.state('chingopedia', {
    	url: '/chingopedia',
		templateUrl: 'templates/chingopedia/index.html',
        controller: 'ChingopediaCtrl'
  	})
  	
  	/* SETTINGS */
  	.state('settings', {
    	url: '/settings',
		templateUrl: 'templates/users/settings.html',
        controller: 'SettingsCtrl'
  	})
  	
  	/* PHOTOFEED */
  	.state('photofeed', {
    	url: '/photofeed',
		templateUrl: 'templates/photofeed/index.html',
        controller: 'PhotofeedCtrl'
  	})
  	
  	.state('photofeeduser', {
    	url: '/photofeeduser',
		templateUrl: 'templates/photofeed/user.html',
        controller: 'PhotofeedUserCtrl'
  	})
  	
  	.state('photofeedcreate', {
    	url: '/photofeedcreate',
		templateUrl: 'templates/photofeed/create.html',
        controller: 'PhotofeedCreateCtrl'
  	})
  	
  	/* LOGOUT */
  	.state('logout', {
    	url: '/logout',
    	templateUrl: 'templates/logout.html',
		controller: 'LogoutCtrl'
  	});
  	
  	$urlRouterProvider.otherwise("/start");
  	
  	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
	
})
