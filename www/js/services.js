angular.module('nikecdmx.services', [])

.factory('TheCity', function() {
	var cities = [
		{
			id: 1,
			name: 'CDMX CITY FACTS',
			thumbnail: 'img/thumbnails/thecity_cdmx_city_facts.jpg',
			view: 'cdmx',
			content: 'The city of Mexico is one of the most important cities of the world.',
		},
		{
			id: 2,
			name: 'HISTORY',
			thumbnail: 'img/thumbnails/thecity_history.jpg',
			view: 'history',
			content: 'Officially known as México, D. F., or simply D. F. is the federal district (distrito federal), capital of Mexico and seat of the federal powers of the union. It is a federal entity within Mexico which is not part of any one of the 31 Mexican states but belongs to the federation as a whole.',
		},
		{
			id: 3,
			name: 'LANDMARKS',
			thumbnail: 'img/thumbnails/thecity_landmarks.jpg',
			view: 'landmarks',
			content: 'Explore the most important landmarks in the city.',
		}
	];

	return {
    	all: function() {
			return cities;
    	},
		get: function(cityId) {
			for (var i = 0; i < cities.length; i++) {
				if (cities[i].id === parseInt(cityId)) {
					return cities[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('CDMX', function() {
	var numbers = [
		{
			id: 1,
			name: 'demographics',
			title: 'img/titles/cdmx/demographics.jpg',
			items: {
				0: {
					'graph': 'img/graphs/cdmx/demographics/01.png'
				},
				1: {
					'graph': 'img/graphs/cdmx/demographics/02.png'
				},
				2: {
					'graph': 'img/graphs/cdmx/demographics/03.png'
				},
				3: {
					'graph': 'img/graphs/cdmx/demographics/04.png'
				},
			}
		},
		{
			id: 2,
			name: 'economics',
			title: 'img/titles/cdmx/economics.jpg',
			items: {
				0: {
					'graph': 'img/graphs/cdmx/economics/01.png'
				},
				1: {
					'graph': 'img/graphs/cdmx/economics/02.png'
				},
				2: {
					'graph': 'img/graphs/cdmx/economics/03.png'
				},
				3: {
					'graph': 'img/graphs/cdmx/economics/04.png'
				},
			}
		},
		{
			id: 3,
			name: 'hyperconnected',
			title: 'img/titles/cdmx/hyperconnectivity.png',
			items: {
				0: {
					'graph': 'img/graphs/cdmx/hyperconnected/01.png'
				},
				1: {
					'graph': 'img/graphs/cdmx/hyperconnected/02.png'
				},
				2: {
					'graph': 'img/graphs/cdmx/hyperconnected/03.png'
				},
				3: {
					'graph': 'img/graphs/cdmx/hyperconnected/04.png'
				},
			}
		},
		{
			id: 4,
			name: 'stateoftheplay',
			title: 'img/titles/cdmx/stateoftheplay.jpg',
			items: {
				0: {
					'graph': 'img/graphs/cdmx/stateoftheplay/01.png'
				},
				1: {
					'graph': 'img/graphs/cdmx/stateoftheplay/02.png'
				},
				2: {
					'graph': 'img/graphs/cdmx/stateoftheplay/03.png'
				},
				3: {
					'graph': 'img/graphs/cdmx/stateoftheplay/04.png'
				},
				4: {
					'graph': 'img/graphs/cdmx/stateoftheplay/05.png'
				},
				5: {
					'graph': 'img/graphs/cdmx/stateoftheplay/06.png'
				}
			}
		},
		{
			id: 5,
			name: 'stateoftheindustry',
			title: 'img/titles/cdmx/stateoftheindustry.jpg',
			items: {
				1: {
					'graph': 'img/graphs/cdmx/stateoftheindustry/01.png'
				},
				2: {
					'graph': 'img/graphs/cdmx/stateoftheindustry/02.png'
				},
				3: {
					'graph': 'img/graphs/cdmx/stateoftheindustry/03.png'
				},
			}
		}
	];

	return {
    	all: function() {
			return numbers;
    	},
		get: function(numberId) {
			for (var i = 0; i < numbers.length; i++) {
				if (numbers[i].id === parseInt(numberId)) {
					return numbers[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('History', function() {
	var history = [
		{
			id: 1,
			name: 'HISTORY OF MEXICO CITY',
			content: '<p><b>Early History</b></p>' +
						'<p>Mexico City is located in a valley that was inhabited by several indigenous groups from 100 to 900 A.D. These tribes were related to the Toltecas, who established Tula in approximately 850 A.D. in the modern-day state of Hidalgo. When the Toltecas declined in power and influence, the Acolhula, Chichimeca and Tepenaca cultures rose up in their place.</p>' +
						'<p>Teotihuacán was founded in 1325 A.D. by the Mexicas. Its development fulfilled one of their ancient prophecies: The Mexicas believed that their god would show them where to build a great city by providing a sign, an eagle eating a snake while perched atop a cactus. When the Mexicas (who would later be known as the Aztecs) saw the vision come true on an island in Lake Texcoco, they decided to build a city there.</p>' +
						'<p>The Aztecs were fierce warriors who eventually dominated other tribes throughout the region. They took what was once a small natural island in the Lake Texcoco and expanded it by hand to create their home and fortress, the beautiful Tenochtitlán. Their civilization, like their city, eventually became the largest and most powerful in pre-Columbian America.</p>' +
						'<p><b>Middle History</b></p>' +
						'<p>Skilled warriors, the Aztecs dominated all of Mesoamerica during this era, making some allies but even more enemies. When Spanish explorer Hernán Cortés made it clear in 1519 that he intended to conquer the area, many local chieftains seized the opportunity to liberate themselves from Aztec rule and joined his army. When Cortés and his allies arrived in the area, Moctezuma II believed that the Spaniard was (or was related to) the god Quetzalcóatl, whose return had been prophesied. Moctezuma sent gifts to the Spanish, hoping they would depart and spare his city. Undaunted, Cortés marched his army to the city and entered it. Not wishing to offend a god, Moctezuma welcomed Cortés and his soldiers into the city and extended every courtesy. After enjoying the king’s hospitality for several weeks, Cortés suddenly ordered that the emperor be placed under house arrest, intending to use him to gain leverage with the Aztecs. For months after, Moctezuma continued to appease his captors, losing most of his subjects’ respect in the process. In 1520, Cortés and his troops conquered the Teotihuacán. The Spanish then built Mexico City on the ruins of the once great city.</p>' +
						'<p>During the colonial period (1535-1821), Mexico City was one of the most important cities in the Americas. Although the native Indians needed work permits to enter the Spanish-dominated city, the population inevitably intermingled and created the Mestizo class, mixed-blood citizens who eventually became a political force. During the 16th and 17th centuries, the caste system prevailed in Mexico City, separating the population into complex ethnic divisions including the Mestizos, Criollos and Coyotes. The Catholic Church had great influence in the city, and religious orders like the Franciscans, Marists and Jesuits established convents and missions throughout Mexico.</p>' +
						'<p>The Spanish Crown’s power relied on the support and loyalty of New Spain’s aristocracy. Political power remained in the hands of the Spaniards born in Spain, but by the 18th century, the Criollo class (descendants of the Spanish who were born in the Americas) had grown in number and social power. The struggle for recognition and favor among the various classes drew attention to the country’s political corruption and helped spark the independence movement.</p>' +
						'<p>The catalyst for Mexico’s independence was a Catholic priest named Miguel Hidalgo y Costilla, who made the first public cry for rebellion in Dolores, Hidalgo, in 1810. Hidalgo had begun attending meetings of educated criollos who were agitating for a large-scale uprising of mestizos and indigenous peasants. Discontent with Spanish rule was spreading rapidly throughout the country. When rumors of military intervention by the Spanish began, the priest decided it was time to act. Parishioners who came to hear mass on Sunday, September 16, 1810, instead heard a call to arms.</p>' +
						'<p>Sparked by the energy of the grassroots rebellion, militant revolutionary armies quickly formed under the leadership of men like Guadalupe Victoria and Vicente Guerrero. The War of Independence lasted 11 years. In 1821, the last Viceroy of New Spain, Juan O’Donoju, signed the Plan of Iguala, which granted Mexico independence.</p>' +
						'<p><b>Recent History</b></p>' +
						'<p>When Mexico’s Distrito Federal (Federal District, also known as Mexico D.F.) was created in 1824, it originally encompassed Mexico City and several other municipalities. As Mexico City grew, it became one large urban area. In 1928, all other municipalities within the Distrito Federal were abolished except Mexico City, making it by default the country’s Distrito Federal. In 1993, the 44th Article of the Constitution of Mexico officially declared Mexico City and the Distrito Federal to be a single entity.</p>' +
						'<p>In 1846, after two decades of peace, Mexico City was invaded by the United States during the Mexican-American War. Under the Treaty of Guadalupe Hidalgo, which ended the war in 1848, Mexico was forced to cede a wide swath of its northern territory to the United States. Today, that territory makes up the U.S. states of New Mexico, Nevada, Colorado, Arizona, California and portions of Utah and Wyoming. Mexico was also forced to recognize the independence of Texas.</p>' +
						'<p>On July 17, 1861, Mexican President Benito Juárez suspended all interest payments to Spain, France and Britain, who launched a combined assault on Veracruz in January 1862. When Britain and Spain withdrew their forces, the French took control of the country. Supported by Mexican conservatives and by French Emperor Napoleon III, Maximiliano de Hamburgo arrived in 1864 to rule Mexico. His policies were more liberal than expected, but he soon lost Mexican support and was assassinated on June 19, 1867, when the liberal government of Benito Juárez regained Mexico’s leadership of the country.</p>' +
						'<p>On November 29, 1876, Porfirio Díaz appointed himself president. He served one term and ushered in his hand-picked successor, Manuel González, whose presidency was marked by corruption and official incompetence. Díaz was then re-elected and saw to it that the constitution was amended to allow two terms in office with unlimited re-elections. A cunning and manipulative politician, Díaz maintained power for the next 36 years through violence, election fraud and repression, even assassination, of his opponents.</p>' +
						'<p>By 1910, the citizenry had lost patience with Díaz’s self-serving leadership and unwillingness to recognize minority rights. On November 20 of that year, Francisco Madero issued the Plan de San Luis Potosí, which declared the Díaz regime illegal and initiated a revolution against the president. Forces led by Francisco Villa, Emiliano Zapata and Venustiano Carranza supported Madero’s bid for the presidency, and Díaz reluctantly agreed to step aside in 1911. Political turmoil and power exchanges continued for over a decade, ending with the establishment of the Partido Nacional Revolucionario party (today’s PRI), which ushered in a period of stability for Mexico City and the rest of the country that lasted until 2000.</p>' +
						'<p><b>MEXICO CITY TODAY</b></p>' +
						'<p>Today, Mexico City is the political, economic and social hub of Mexico and the largest metropolitan area in the Western Hemisphere. The city’s nominal gross domestic product per capita is $17,696, the highest of any city in Latin America. However, the distribution of the wealth is extremely uneven, and a full 15 percent of the city’s residents live in poverty.</p>' +
						'<p>Labor unions for taxi drivers, telephone workers and electrical workers are very strong in Mexico City. Many of these unions are linked to the PRI political party, but recently, some unions have begun shifting their loyalty towards the Partido de la Revolución Democrática (Party of the Democratic Revolution), which has ruled the city since 1997.</p>' +
						'<p>Some of Mexico City’s best-known neighborhoods are the artsy Coyoacan (home of the Frida Kahlo Museum), the upscale Santa Fe (including the Bosques de las Lomas area), the old fashioned Xochimilco (Mexico’s Little Venice) and the elegant Polanco.</p>',
			gallery: {
				0: 'img/gallery/history/01.jpg',
				1: 'img/gallery/history/02.jpg',
				2: 'img/gallery/history/03.jpg',
			}
		}
	];

	return {
    	all: function() {
			return history;
    	},
		get: function(historyId) {
			for (var i = 0; i < history.length; i++) {
				if (history[i].id === parseInt(historyId)) {
					return history[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('Landmarks', function() {
	var landmarks = [
		{
			id: 1,
			name: 'ZÓCALO',
			icon: 'img/icons/icon_zocalo.png',
			content: '<p>The main plaza of Mexico City is commonly known as the Zócalo, meaning plinth. It was the main ceremonial center in the Aztec city of Tenochtitlan and from the colonial period on, the main plaza or square in the heart of the historic center of Mexico City.</p>' +
						'<p>The plaza used to be known simply as the “Main Square” or “Arms Square,” and today its formal name is Plaza de la Constitución (Constitution Square). This name does not come from any of the Mexican constitutions that have governed the country but rather from the Cádiz Constitution which was signed in Spain in 1812.</p>' +
						'<p>However, it is almost always called the Zócalo today. Plans were made to erect a column as a monument to Independence, but only the base, or zócalo, was ever built. The plinth was destroyed long ago but the name has lived on. Many other Mexican towns and cities, such as Oaxaca and Guadalajara, have adopted the word zócalo to refer to their main plazas, but not all.</p>' +
						'<p>It has been a gathering place for Mexicans since Aztec times, having been the site of Mexican ceremonies, the swearing in of viceroys, royal proclamations, military parades, independence ceremonies and modern religious events such as the festivals of Holy Week and Corpus Christi. It has received foreign heads of state and is the main venue for both national celebration and national protest. The Zócalo and surrounding blocks have played a central role in the city’s planning and geography for almost 700 years. The site is just one block southwest of the Templo Mayor which, according to Aztec legend and mythology, was considered the center of the universe.</p>' +
						'<p>It is bordered by the Cathedral to the north, the National Palace to the east, the Federal District buildings to the south and El Portal de Mercaderes to the west, the Nacional Monte de Piedad building at the north-west corner, with the Templo Mayor site to the northeast, just outside of view.</p>' +
						'<p>In the centre is a flagpole with an enormous Mexican flag ceremoniously raised and lowered each day and carried into the National Palace. There is an entrance to the Metro station “Zócalo” located at the northeast corner of the square but no sign above ground indicates its presence.</p>',
			gallery: {
				0: 'img/gallery/zocalo/01.jpg',
				1: 'img/gallery/zocalo/02.jpg',
				2: 'img/gallery/zocalo/03.jpg'
			}
		},
		{
			id: 2,
			name: 'ÁNGEL DE LA INDEPENDENCIA',
			icon: 'img/icons/icon_angel.png',
			content: '<p>The Column of Independence is one of the great symbols of our nation and is located in the largest roundabout in the beautiful Paseo de la Reforma in Mexico City. Its main side is oriented toward the center of the city. The column alone is 36 meters high, considering the sculptural angel, it reaches a total height of 45 meters, which is equivalent to a 15-story building.</p>' +
						'<p>The famous architect Antonio Rivas Mercado was the author of the project, while the engineer Roberto Gayol conducted and directed the work and the Italian artist Enrique Alciati was responsible for the sculptures.</p>' +
						'<p>The first stone for the construction was placed on January 2, 1902, more than 100 years ago, and the ceremony was led by Porfirio Diaz.</p>' +
						'<p>The completed monument was inaugurated by Porfirio Diaz on September 16, 1910, to celebrate the centennial of the Independence of Mexico. Its mausoleum preserves the remains of 14 heroes of that heroic war.</p>' +
						'<p>This sculpture fell to the ground during an earthquake in the morning of July 28, 1957 and was rebuilt and restored by a group of experts led by the sculptor José María Fernández Urbina. This work took more than a year to be completed, so the column was fully restored until September 16 of 1958.</p>' +
						'<p>On the front of the column there is a white marble plaque which reads: “La Nación para los Héroes de la Independencia”, which in English means "The Nation for the Independence Heroes". In front of this inscription is a giant bronze lion led by a child that symbolizes ”strong in war and docile in peace”.</p>' +
						'<p>The sculpture is located at the top of the column and is known as the Angel of Independence, it represents the Winged Victory. It is made of bronze with gold plating and stands 6.7 meters high and weighs 7 tons. One hand holds the crown laurel symbolizing victory and the other a chain with broken links, symbolizing the end of slavery imposed during three centuries by Spain.</p>',
			gallery: {
				0: 'img/gallery/angel/01.jpg',
				1: 'img/gallery/angel/02.jpg',
				2: 'img/gallery/angel/03.jpg'
			}
		},
		{
			id: 3,
			name: 'BELLAS ARTES',
			icon: 'img/icons/icon_bellasartes.png',
			content: '<p>The Bellas Artes Palace is the most important cultural center in Mexico City as well as the rest of the country of Mexico. It is located on the west side of the historic center of Mexico City next to the Alameda central park.</p>' +
						'<p>The first National Theater of Mexico was built in the late 19th century, but it was soon decided to tear this down in favor of a more opulent building in time for the Centennial of the Mexican War of Independence in 1910.</p>' +
						'<p>The initial design and construction was undertaken by Italian architect Adamo Boari in 1904, but complications arising from the soft subsoil and political problems, both before and during the Mexican Revolution, hindered and then stopped construction completely by 1913.</p>' +
						'<p>Construction began again in 1932 under Mexican architect Federico Mariscal and was completed in 1934. The exterior of the building is primarily Neoclassical and Art Nouveau and the interior is primarily Art Deco.</p>' +
						'<p>The building is best known for its murals by Diego Rivera, Siqueiros and others, as well as the many exhibitions and theatrical performances its hosts, including the Ballet Folklórico de Mexico.</p>',
			gallery: {
				0: 'img/gallery/bellasartes/01.jpg',
				1: 'img/gallery/bellasartes/02.jpg',
				2: 'img/gallery/bellasartes/03.jpg'
			}
		},
		{
			id: 4,
			name: 'CHAPULTEPEC',
			icon: 'img/icons/icon_chapultepec.png',
			content: '<p>Chapultepec, more commonly called the "Bosque de Chapultepec" in Mexico City, is one of the largest city parks in the Western Hemisphere, measuring in total just over 686 hectares (1,695 acres). Centered on a rock formation called Chapultepec Hill, one of the park\'s main functions is to be an ecological space in the vast megalopolis.</p>' +
						'<p>It is considered the first and most important of Mexico City\'s "lungs", with trees that replenish oxygen to the Valley of Mexico.</p>' +
						'<p>The park area has been inhabited and held as special since the Pre-Columbian era, when it became a retreat for Aztec rulers. In the colonial period, Chapultepec Castle was built here, eventually becoming the official residence of Mexican heads of state. It would remain such until 1940, when it was moved to another part of the park called Los Pinos.</p>' +
						'<p>Today, the park is divided into three sections, with the first section being the oldest and most visited. This section contains most of the park\'s attractions including its zoo, the Museum of Anthropology, the Rufino Tamayo Museum, and more. It receives an estimated 15 million visitors per year. This prompted the need for major rehabilitation efforts which began in 2005 and ended in 2010.</p>',
			gallery: {
				0: 'img/gallery/chapultepec/01.jpg',
				1: 'img/gallery/chapultepec/02.jpg',
				2: 'img/gallery/chapultepec/03.jpg'
			}
		},
		{
			id: 5,
			name: 'COYOACÁN',
			icon: 'img/icons/icon_coyoacan.png',
			content: '<p>Coyoacán refers to one of the 16 boroughs of the Federal District of Mexico City as well as the former village which is now the borough’s “historic center.” The name comes from Nahuatl and most likely means “place of coyotes,” when the Aztecs named a pre-Hispanic village on the southern shore of Lake Texcoco which was dominated by the Tepanec people.</p>' +
						'<p>Against Aztec domination, these people welcomed Hernán Cortés and the Spanish, who used the area as headquarters during the Spanish conquest of the Aztec Empire and made it the first capital of New Spain between 1521 and 1523.</p>' +
						'<p>The village, later municipality, of Coyoacán remained completely independent of Mexico City through out the colonial period into the 19th century. In 1857, the area was incorporated into the Federal District when this district was expanded.</p>' +
						'<p>In addition Coyoacán hosts a number of cultural expressions on its main square known as Centro de Coyoacán, where to name a few, you can find mimes, a traditional organ and the unique hot chocolate and churros.</p>' +
						'<p>Through out its history, Coyoacán has witnessed the passage of important painters, writers, artists and sculptors whose contribution has enriched the cultural heritage of our country. Diego Rivera, Frida Kahlo, Salvador Novo, Octavio Paz, Leon Trotsky, Emilio El Indio Fernandez are some of its illustrious inhabitants.</p>' +
						'<p>Coyoacán has some large and beautiful forests, such as “Viveros” or “Huayamilpas” park, to name just two.</p>' +
						'<p>Within the limits of this district is the National Autonomous University of Mexico, the largest in Latin America.</p>' +
						'<p>This has made the borough of Coyoacán, especially its historic center, a popular place to visit on weekends.</p>',
			gallery: {
				0: 'img/gallery/coyoacan/01.jpg',
				1: 'img/gallery/coyoacan/02.jpg',
				2: 'img/gallery/coyoacan/03.jpg'
			}
		},
		{
			id: 6,
			name: 'XÓCHIMILCO',
			icon: 'img/icons/icon_xochimilco.png',
			content: '<p>Xochimilco is one of the 16 boroughs within the Mexican Federal District. The borough is centered on the formerly independent city of Xochimilco, which was established on what was the southern shore of Lake Xochimilco in the pre-Hispanic period. Today, the borough consists of the eighteen “barrios” or neighborhoods of this city along with fourteen “pueblos” or villages that surround it covering an area of 125 km&sup2; (48 sq mi).' +
						'<p>While the borough is somewhat in the geographic center of the Federal District, it is considered to be “south” and has an identity separate from the historic center of Mexico City.</p>' +
						'<p>This is due to its historic separation from that city during most of its history. Xochimilco is best known for its canals, which are left from what was an extensive lake and canal system that connected most of the settlements of the Valley of Mexico. These canals, along with artificial islands called chinampas, attract tourists and other city residents to ride on colorful gondola-like boats called “trajineras” around the 170 km (110 mi) of canals.</p>' +
						'<p>This canal and chinampa system, as a vestige of the area’s pre-Hispanic past, has made Xochimilco a World Heritage Site. In 1950, Paramahansa Yogananda in his celebrated classic Autobiography of a Yogi wrote that if there is a scenic beauty contest, Xochimilco will get the First Prize. However, severe environmental degradation of the canals and chinampas has brought that status into question.</p>',
			gallery: {
				0: 'img/gallery/xochimilco/01.jpg',
				1: 'img/gallery/xochimilco/02.jpg',
				2: 'img/gallery/xochimilco/03.jpg'
			}
		},
		{
			id: 7,
			name: 'SANTA FE',
			icon: 'img/icons/icon_santafe.png',
			content: '<p>Santa Fe is one of Mexico City\'s major business districts, located in the west part of the city in the boroughs of Cuajimalpa and Álvaro Obregón. Paseo de la Reforma and Constituyentes are the primary means of access to the district from the central part of the city.</p>' +
						'<p>Santa Fe consists mainly of highrise buildings surrounding a large shopping mall, which is currently the third largest mall in Latin America: Santa Fe Center. The district also includes a residential area and three college campuses, among other facilities.</p>' +
						'<p>Santa Fe is the most important financial district of the city, where major international companies have their local or regional offices, including Banco Santander and Grupo Financiero Banamex, Harley Davison, Ford Motor Company, Coca Cola foods - FEMSA, and more. Santa Fe has a large hotel infrastructure, exclusive and very new hotels like JW Marriott, Hilton, Westin and other make Santa Fe one of the most attractive areas of the city for long stays, in addition to documentation centers for several airlines.</p>' +
						'<p>One of the things that distinguishes Santa Fe from the rest of the country is the high concentration of tall, new buildings. There are at least 20 buildings standing over 100 meters, some with a height of 161 meters.</p>' +
						'<p>Santa Fe also has great commercial activity, which initially revolved around Centro Santa Fe mall whose size has become a tourist attraction in the area. Other malls such as Zentrika were built afterwards, Samara and Garden Santa Fe, which is the first sustainable commercial center.</p>',
			gallery: {
				0: 'img/gallery/santafe/01.jpg',
				1: 'img/gallery/santafe/02.jpg',
				2: 'img/gallery/santafe/03.jpg'
			}
		},
		{
			id: 8,
			name: 'SATÉLITE',
			icon: 'img/icons/icon_satelite.png',
			content: '<p>Ciudad Satélite, frequently called just Satélite is a Greater Mexico City middle-class suburban area located in Naucalpan, State of Mexico. Officially the name corresponds only to the homonym neighborhood, Ciudad Satélite, founded on1957. Over the last few decades it has expanded and it is now rather segmented socially and economically, distributed in a number of different neighborhoods, although it has remained a predominantly middle to upper middle class area.</p>' +
						'<p>Satélite originally was conceived as a commuter bedroom community; developers hoped to maintain a green belt between it and Mexico City, but its rapid development (and rising property prices) made this untenable. However, popular culture, market segmentation, availability of services, and comings and goings of life in this area have helped to define Satélite as a cultural center.</p>' +
						'<p>Ciudad Satélite became the core of a new suburban phenomenon that eventually included not only single-family dwellings but also apartment buildings, condominiums, and retail spaces; some manufacturing also developed. This was a vast departure from the original concept. Over time, the progress of real estate development has expanded on the original meaning of the Satélite community in the minds of Mexico City residents.</p>' +
						'<p>The project was approved by the president Miguel Alemán Valdés in 1948. The city was almost finished, but remained uninhabited until 1952 when people started to move in because of the attractive prices. Public services such as the phone lines were not finished yet in all circuits and people initially had to use public phones. By the 1970s, the Ciudad Satélite population vastly increased.</p>',
			gallery: {
				0: 'img/gallery/satelite/01.jpg',
				1: 'img/gallery/satelite/02.jpg',
				2: 'img/gallery/satelite/03.jpg'
			}
		},
		{
			id: 9,
			name: 'EL SOPE',
			icon: 'img/icons/icon_elsope.png',
			content: '<p>The track “El Sope” is located in the Bosque de Chapultepec, along Av. Bosques, located on the hill of Chapultepec next to the ring road “Anillo Periférico” in Mexico City.</p>' +
						'<p>El Sope is not a flat track, on the contrary, since the Chapultepec Park is nestled in what was the Cerro de Chapultepec, El Sope offers a gravel path with many ups and downs, which make it a pleasant journey and a good challenge for runners and walkers who visit.</p>' +
						'<p>This track receives daily an average of 1,000 runners and is open 365 days a year, today it is the most valuable place for the entire athletic community that visits it. It promotes a healthy lifestyle in the inhabitants of the city.</p>' +
						'<p>It features:<br/>Track 1 of 820 mts.<br/>Track 2 of 1150 mts.<br/>Speed Track of 100 mts.<br/>Signaling every 100 mts.<br/>Lighting network.<br/>Irrigation network.<br/>Gardening.<br/>Perimeter fence.<br/>Outdoor Gym.</p>' +
						'<p>Rehabilitation of the track took place during 2010 - 2011 and was an innovative and self-sustaining project, part of the management plan for the rescue of the Second Section of Chapultepec, a program intended to improve the physical conditions of the 2nd Section of Chapultepec Park.</p>' +
						'<p>El Sope has a small area with different equipment for stretching and another area with apparatuses for performing strength exercises with your own weight; and various kiosks or areas where different activities like zumba and yoga classes and small bathrooms. And, as in the different tracks running along the city in El Sope, you can meet different teams of runners, and join if you wish.</p>' +
						'<p>Running in El Sope is an excellent opportunity to train while enjoying the scents and colors that nature offers and to strengthen your legs and improve your condition; all in a friendly environment, clean and, most importantly, safely.</p>',
			gallery: {
				0: 'img/gallery/elsope/01.jpg',
				1: 'img/gallery/elsope/02.jpg',
				2: 'img/gallery/elsope/03.jpg'
			}
		},
		{
			id: 10,
			name: 'ESTADIO OLÍMPICO UNIVERSITARIO (CU)',
			icon: 'img/icons/icon_unam.png',
			content: '<p>On August 7, 1950 the story begins. That day, in a ravine located in the Pedregal de San Ángel, the first stone of what is now the majestic Estadio Olímpico Universitario was placed & on November 20, 1952 it was opened to host the II National Youth Games.</p>' +
						'<p>The Estadio Olímpico Universitario holds 68,954 spectators and has parking spaces to hold 2,618 vehicles.</p>' +
						'<p>The main facade is decorated with a polychrome relief mural, entitled “La Universidad, La Familia y el Deporte en México”, which in English means "University, Family and Sport in Mexico", by Mexican artist Diego Rivera, who was to define his monumental creation as the most important achievement of his life.</p>' +
						'<p>The Olympic Stadium has hosted many different sports events such as the national finals of the Youth Sports Games, the college football classic Poli-University and the XIX Olympic Games in 1968. It was precisely in this competition where a long jump of 8.90 m by American athlete Bob Beamon set a world record that lasted for over 20 years. This is why the Estadio Olímpico Universitario has great importance in the sports world.</p>' +
						'<p>The Olympic Stadium was one of the venues for the Soccer World Championship in 1986. The stadium, at different times, has received world football legends like Pele and Maradona. Outstanding soccer players like Enrique Borja and Hugo Sanchez emerged in this field.</p>' +
						'<p>Currently the Estadio Olímpico Universitario is home of first division soccer team Pumas,  representing the Universidad Nacional Autónoma de México (UNAM).</p>',
			gallery: {
				0: 'img/gallery/cu/01.jpg',
				1: 'img/gallery/cu/02.jpg',
				2: 'img/gallery/cu/03.jpg'
			}
		},
		{
			id: 11,
			name: 'ESTADIO AZTECA',
			icon: 'img/icons/icon_azteca.png',
			content: '<p>The Estadio Azteca is a football stadium located in the suburb of Santa Ursula in Mexico City, Mexico. Since its opening in 1966, the stadium has been the official home stadium of the professional football team Club América and the official national stadium of the Mexico national football team. With an official capacity of 105,064, it is the largest stadium in Mexico, the fourth-largest in the American continent, the sixth-largest in the world and the largest association football-specific stadium in the world.</p>' +
						'<p>Regarded as one of the most famous and iconic football stadiums in the world, it is the first to have hosted two FIFA World Cup Finals. In the 1970 World Cup Final, Brazil defeated Italy 4–1 and in the 1986 World Cup Final, Argentina defeated West Germany 3–2.</p>' +
						'<p>It also hosted the 1986 quarter-final match between Argentina and England in which Diego Maradona scored both the “Hand of God goal” and the “Goal of the Century”. The stadium also hosted the “Game of the Century”, when Italy defeated West Germany 4–3 in extra time in one of the 1970 World Cup semifinal matches.</p>' +
						'<p>The stadium was also the principal venue for the football tournament of the 1968 Summer Olympics.</p>' +
						'<p>The stadium has been given the nickname “Co- loso de Santa Ursula” which in English means “Colossus of Saint Ursula”, due to its large structure. Santa Ursula refers to the part of town where the stadium resides in Mexico City.</p>',
			gallery: {
				0: 'img/gallery/azteca/01.jpg',
				1: 'img/gallery/azteca/02.jpg',
				2: 'img/gallery/azteca/03.jpg'
			}
		},
		{
			id: 12,
			name: 'PASEO DE LA REFORMA',
			icon: 'img/icons/icon_reforma.png',
			content: '<p>Paseo de la Reforma is a wide avenue that runs diagonally across the heart of Mexico City. It was designed by Ferdinand Von Rosenzweig in the 1860’s and modeled after the great boulevards of Europe, such as the Ringstrasse in Vienna and the Champs-Élysées in Paris.</p>' +
						'<p>After the French intervention in Mexico overthrew the constitutional President Benito Juárez, the newly crowned Emperor Maximilian made his mark on the conquered city. He commissioned a grand avenue linking the city center with his imperial residence, Chapultepec Castle, which was then on the southwestern edge of town.</p>' +
						'<p>The project was originally named Paseo de la Emperatriz ("Promenade of the Empress") in honor of Maximilian\'s consort and second cousin Empress Carlota.</p>' +
						'<p>After her return to Europe and Maximilian\'s subsequent execution, the restored Juárez government renamed the Paseo in honor of the Reform War.</p>' +
						'<p>It is now home to many of Mexico\'s tallest buildings such as the Torre Mayor and others in the Zona Rosa. More modern extensions continue the avenue at an angle to the old Paseo. To the northeast it continues toward Tlatelolco, where it changes its name near the Plaza de las Tres Culturas.</p>' +
						'<p>There it splits into Calzada de Guadalupe and Calzada de los Misterios which continues toward La Villa. Its western portion going west from Chapultepec Park passes south of Polanco on its way through the exclusive neighborhood of Lomas de Chapultepec and then into Cuajimalpa and Santa Fe on the outskirts of the city, although when it reaches this point it is more a highway than a promenade.</p>',
			gallery: {
				0: 'img/gallery/reforma/01.jpg',
				1: 'img/gallery/reforma/02.jpg',
				2: 'img/gallery/reforma/03.jpg'
			}
		},
	];

	return {
    	all: function() {
			return landmarks;
    	},
		get: function(landmarkId) {
			for (var i = 0; i < landmarks.length; i++) {
				if (landmarks[i].id === parseInt(landmarkId)) {
					return landmarks[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('TheBrand', function() {
	var graps = [
		{
			id: 1,
			name: 'Innovative',
			graph: 'img/graphs/thebrand/01.PNG'
		},
		{
			id: 2,
			name: 'Insipires Me',
			graph: 'img/graphs/thebrand/02.PNG'
		},
		{
			id: 3,
			name: 'Marketshare in CDMX',
			graph: 'img/graphs/thebrand/03.PNG'
		},
		{
			id: 4,
			name: 'Performance',
			graph: 'img/graphs/thebrand/04.PNG'
		},
	];

	return {
    	all: function() {
			return graps;
    	},
		get: function(graphId) {
			for (var i = 0; i < graps.length; i++) {
				if (graps[i].id === parseInt(graphId)) {
					return graps[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('TheConsumer', function() {
	var consumers = [
		{
			id: 1,
			name: 'GENIUS FOT',
			thumbnail: 'img/thumbnails/theconsumer_geniusfot.jpg',
			content: 'They are able to overcome all challenges and will keep latin football beautifull, passionate, creative and unpredictable. Inspired by global & local athletes.',
			map_one: 'img/maps/geniusfot_wcl.png',
			map_two: 'img/maps/geniusfot_wcp.png',
			map_three: 'img/maps/geniusfot_wcs.png'
		},
		{
			id: 2,
			name: 'Y&F GENIUS RUNNER',
			thumbnail: 'img/thumbnails/theconsumer_geniusrunner.jpg',
			content: 'Goes against the city schedule. Fighting his will he runs while everyone else is sleeping. Inspired by his closest circle & local athletes.',
			map_one: 'img/maps/geniusrunner_wcl.png',
			map_two: 'img/maps/geniusrunner_wcp.png',
			map_three: 'img/maps/geniusrunner_wcs.png'
		},
		{
			id: 3,
			name: 'Y&F GENIUS NSW',
			thumbnail: 'img/thumbnails/theconsumer_geniusnsw.jpg',
			content: 'Connected, stylish, resourceful. He values his roots, his culture and the handcrafting that has shaped chilangos generations, inspired by global & local influencers / athletes, through sports & culture.',
			map_one: 'img/maps/geniusnsw_wcl.png',
			map_two: 'img/maps/geniusnsw_wcs.png',
			map_three: ''
		},
		{
			id: 4,
			name: 'Y&F GENIUS MGOSF',
			thumbnail: 'img/thumbnails/theconsumer_gmgof.jpg',
			content: 'She is proud, trendy, always ready to accept the challenge. Despite her activeness she doesn’t consider herself an athlete. Inspired by global athletes & influenced by her closest circles.',
			map_one: 'img/maps/geniusgmgof_wcl.png',
			map_two: 'img/maps/geniusgmgof_wcp.png',
			map_three: 'img/maps/geniusgmgof_wcs.png'
		},
	];

	return {
    	all: function() {
			return consumers;
    	},
		get: function(consumerId) {
			for (var i = 0; i < consumers.length; i++) {
				if (consumers[i].id === parseInt(consumerId)) {
					return consumers[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('TheMarketplace', function() {
	var marketplaces = [
		{
			id: 1,
			name: 'OUR LEADING RETAIL CONCEPTS',
			thumbnail: 'img/thumbnails/themarketplace_ourleadingretailconcepts.jpg',
			view: 'ourleadingretail'
		},
		{
			id: 2,
			name: 'THE MUST SEE DOORS',
			thumbnail: 'img/thumbnails/themarketplace_themustseedoors.jpg',
			view: 'themustseedoors'
		}
	];

	return {
    	all: function() {
			return marketplaces;
    	},
		get: function(marketplaceId) {
			for (var i = 0; i < marketplaces.length; i++) {
				if (marketplaces[i].id === parseInt(marketplaceId)) {
					return marketplaces[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('TheCityCenter', function() {
	var thecitycenter = [
		{
			id: 1,
			name: 'AREA',
			title: 'img/titles/area.jpg',
			name: 'THE CHILANGO´S HEART',
			content: '<p>Our historic center has been designated by UNESCO as Intangible Cultural Heritage of Humanity. It spans 668 blocks, 6 of them belong to Madero Street, which receives 220,000 pedestrians everyday and it´s considered the top commercial street in Latin America. Polanco, a city center district, is the most luxurious zone in the capital and the largest contributor to the country’s gdp(3%).</p>'
		},
		{
			id: 2,
			name: 'POPULATION',
			title: 'img/titles/population.jpg',
			graphs: {
				0: 'img/graphs/consumptonmap/center/01.png',
				1: 'img/graphs/consumptonmap/center/02.png',
				2: 'img/graphs/consumptonmap/center/03.png',
				3: 'img/graphs/consumptonmap/center/04.png',
				4: 'img/graphs/consumptonmap/center/05.png',
				5: 'img/graphs/consumptonmap/center/06.png',
			}
		},
		{
			id: 3,
			name: 'TRADE ZONES',
			title: 'img/titles/tradezones.jpg',
			name: '<u>current</u><br/>city center',
			content: '<p>DOWNTOWN<br/><br/>REFORMA<br/><br/>LA CONDESA<br/><br/>LA ROMA<br/><br/>POLANCO</p>'
		},
	];

	return {
    	all: function() {
			return thecitycenter;
    	}
  	};
})

.factory('South', function() {
	var south = [
		{
			id: 1,
			name: 'AREA',
			title: 'img/titles/area.jpg',
			name: 'THE UNIVERSITY DISTRICT',
			content: '<p>The wellness zone of the city, home turf of UNAM (the biggest university in Latin America), the south of the city, including Coyoacán town and Azteca stadium, is a required stopping point for anyone who is visiting the city</p>'
		},
		{
			id: 2,
			name: 'POPULATION',
			title: 'img/titles/population.jpg',
			graphs: {
				0: 'img/graphs/consumptonmap/south/01.png',
				1: 'img/graphs/consumptonmap/south/02.png',
				2: 'img/graphs/consumptonmap/south/03.png',
				3: 'img/graphs/consumptonmap/south/04.png',
				4: 'img/graphs/consumptonmap/south/05.png',
			}
		},
		{
			id: 3,
			name: 'TRADE ZONES',
			title: 'img/titles/tradezones.jpg',
			items: {
				0: {
					name: '<u>current</u>',
					content: '<p>PERISUR<br/><br/>COAPA<br/><br/></p>'
				},
				1: {
					name: '<u>emerging</u>',
					content: '<p>MITIKAH<br/><br/>OASIS COYOACÁN<br/><br/>XOCHIMILCO</p>'
				}
			}
		},
	];

	return {
    	all: function() {
			return south;
    	}
  	};
})

.factory('East', function() {
	var east = [
		{
			id: 1,
			name: 'AREA',
			title: 'img/titles/area.jpg',
			name: 'THE EMERGING ZONE',
			content: '<p>After Mexico City’s 1985 earthquake, the east emerged as the new neighborhood for the citys center’s residents, today it is the most populated zone in the capital with almost 8 million people. Ciudad deportiva “Magdalena Mixhuca” is considered the stronghold of sports in the city, located in the east, it was the olympic park in the olympic games in 1968.</p>'
		},
		{
			id: 2,
			name: 'POPULATION',
			title: 'img/titles/population.jpg',
			graphs: {
				0: 'img/graphs/consumptonmap/east/01.png',
				1: 'img/graphs/consumptonmap/east/02.png',
				2: 'img/graphs/consumptonmap/east/03.png',
				3: 'img/graphs/consumptonmap/east/04.png',
				4: 'img/graphs/consumptonmap/east/05.png',
				5: 'img/graphs/consumptonmap/east/06.png',
			}
		},
		{
			id: 3,
			name: 'TRADE ZONES',
			title: 'img/titles/tradezones.jpg',
			name: '<u>current</u>',
			content: '<p>TEZONTLE<br/><br/>CIUDAD JARDIN</p>'
		},
	];

	return {
    	all: function() {
			return east;
    	}
  	};
})

.factory('West', function() {
	var west = [
		{
			id: 1,
			name: 'AREA',
			title: 'img/titles/area.jpg',
			name: 'THE NEW BUSINESS DISTRICT',
			content: '<p>Inspired by La Defense in Paris, the west is a major business and financial district in the city. Santa Fe and Interlomas (west neighborhoods) consist mainly of highrise buildings, luxury malls and restaurants. 3 of the most recognized private universities in the city are in the west.</p>'
		},
		{
			id: 2,
			name: 'POPULATION',
			title: 'img/titles/population.jpg',
			graphs: {
				0: 'img/graphs/consumptonmap/west/01.png',
				1: 'img/graphs/consumptonmap/west/02.png',
				2: 'img/graphs/consumptonmap/west/03.png',
				3: 'img/graphs/consumptonmap/west/04.png',
				4: 'img/graphs/consumptonmap/west/05.png',
				5: 'img/graphs/consumptonmap/west/06.png',
			}
		},
		{
			id: 3,
			name: 'TRADE ZONES',
			title: 'img/titles/tradezones.jpg',
			items: {
				0: {
					name: '<u>current</u>',
					content: '<p>SANTA FE<br/><br/></p>'
				},
				1: {
					name: '<u>emerging</u>',
					content: '<p>INTERLOMAS<br/><br/></p>'
				}
			}
		},
	];

	return {
    	all: function() {
			return west;
    	}
  	};
})

.factory('North', function() {
	var north = [
		{
			id: 1,
			name: 'AREA',
			title: 'img/titles/area.jpg',
			name: 'THE EMERGING ZONE',
			content: '<p>Satélite, the banner of the northern side of CDMX, was designed to live completly isolated from the chaos of the city. Today it is a city whithin the city, full of middle and upper class neighborhoods, surrounded by retail spaces, local markets and some manufacturing industry.</p>'
		},
		{
			id: 2,
			name: 'POPULATION',
			title: 'img/titles/population.jpg',
			graphs: {
				0: 'img/graphs/consumptonmap/north/01.png',
				1: 'img/graphs/consumptonmap/north/02.png',
				2: 'img/graphs/consumptonmap/north/03.png',
				3: 'img/graphs/consumptonmap/north/04.png',
				4: 'img/graphs/consumptonmap/north/05.png',
				5: 'img/graphs/consumptonmap/north/06.png',
			}
		},
		{
			id: 3,
			name: 'TRADE ZONES',
			title: 'img/titles/tradezones.jpg',
			items: {
				0: {
					name: '<u>current</u>',
					content: '<p>SATELITE<br/><br/></p>'
				},
				1: {
					name: '<u>emerging</u>',
					content: '<p>TOREO<br/><br/></p>'
				}
			}
		},
	];

	return {
    	all: function() {
			return north;
    	}
  	};
})

.factory('NorthEast', function() {
	var northeast = [
		{
			id: 1,
			name: 'AREA',
			title: 'img/titles/area.jpg',
			name: 'THE INDUSTRIAL VALLEY',
			content: '<p>There are more than 3,000 industries located in the northeast side of the city, making the area one of the most industrialized zones in the entire country. There are also more than 15,000 retail businesses, including malls and several large “tianguis” (traditional street markets).</p>'
		},
		{
			id: 2,
			name: 'POPULATION',
			title: 'img/titles/population.jpg',
			graphs: {
				0: 'img/graphs/consumptonmap/northeast/01.png',
				1: 'img/graphs/consumptonmap/northeast/02.png',
				2: 'img/graphs/consumptonmap/northeast/03.png',
				3: 'img/graphs/consumptonmap/northeast/04.png',
				4: 'img/graphs/consumptonmap/northeast/05.png',
				5: 'img/graphs/consumptonmap/northeast/06.png',
			}
		},
		{
			id: 3,
			name: 'TRADE ZONES',
			title: 'img/titles/tradezones.jpg',
			name: '<u>current</u>',
			content: '<p>LINDAVISTA<br/><br/>ECATEPEC</p>'
		},
	];

	return {
    	all: function() {
			return northeast;
    	}
  	};
})

.factory('OurLeadingRetailConcepts', function() {
	var retailers = [
		{
			id: 1,
			name: 'LEAPFROG DIGITAL',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/01.jpg',
					icons: {
						0: 'img/retailers/01/01.png',
						1: 'img/retailers/01/02.png',
						2: 'img/retailers/01/03.png',
						3: 'img/retailers/01/04.png',
						4: 'img/retailers/01/05.png',
						5: 'img/retailers/01/06.png',
					}
				}
			}
		},
		{
			id: 2,
			name: 'LEAD WITH DTC',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/02.jpg',
					icons: {
						0: 'img/retailers/02/01.png',
						1: 'img/retailers/02/02.png',
						2: 'img/retailers/02/03.png',
						3: 'img/retailers/02/04.png',
						4: 'img/retailers/02/05.png',
						5: 'img/retailers/02/06.png',
					}
				}
			}
		},
		{
			id: 3,
			name: 'CATEGORY SPECIALTY',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/03_rsg.png',
					icons: {
						0: 'img/retailers/03_rsg/01.png',
						1: 'img/retailers/03_rsg/02.png',
						2: 'img/retailers/03_rsg/03.png',
						3: 'img/retailers/03_rsg/04.png',
						4: 'img/retailers/03_rsg/05.png',
						5: 'img/retailers/03_rsg/06.png',
					}
				},
				1: {
					title: 'img/thumbnails/ourlandingretail/03_nsw.png',
					icons: {
						0: 'img/retailers/03_nsw/01.png',
						1: 'img/retailers/03_nsw/02.png',
						2: 'img/retailers/03_nsw/03.png',
						3: 'img/retailers/03_nsw/04.png',
						4: 'img/retailers/03_nsw/05.png',
						5: 'img/retailers/03_nsw/06.png',
					}
				}
			}
		},
		{
			id: 4,
			name: 'ACCELERATE CATALIZING CONCEPTS',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/04_innova.png',
					icons: {
						0: 'img/retailers/04_innova/01.png',
						1: 'img/retailers/04_innova/02.png',
						2: 'img/retailers/04_innova/03.png',
						3: 'img/retailers/04_innova/04.png',
						4: 'img/retailers/04_innova/05.png',
						5: 'img/retailers/04_innova/06.png',
					}
				},
				1: {
					title: 'img/thumbnails/ourlandingretail/04_liverpool.png',
					icons: {
						0: 'img/retailers/04_liverpool/01.png',
						1: 'img/retailers/04_liverpool/02.png',
						2: 'img/retailers/04_liverpool/03.png',
						3: 'img/retailers/04_liverpool/04.png',
						4: 'img/retailers/04_liverpool/05.png',
						5: 'img/retailers/04_liverpool/06.png',
					}
				}
			}
		},
		{
			id: 5,
			name: 'IGNITE ATHLETIC SPECIALTY CHANNEL',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/05_innvictus.png',
					icons: {
						0: 'img/retailers/05_innvictus/01.png',
						1: 'img/retailers/05_innvictus/02.png',
						2: 'img/retailers/05_innvictus/03.png',
						3: 'img/retailers/05_innvictus/04.png',
						4: 'img/retailers/05_innvictus/05.png',
						5: 'img/retailers/05_innvictus/06.png',
					}
				},
				1: {
					title: 'img/thumbnails/ourlandingretail/05_taf.png',
					icons: {
						0: 'img/retailers/05_taf/01.png',
						1: 'img/retailers/05_taf/02.png',
						2: 'img/retailers/05_taf/03.png',
						3: 'img/retailers/05_taf/04.png',
						4: 'img/retailers/05_taf/05.png',
						5: 'img/retailers/05_taf/06.png',
					}
				}
			}
		},
		{
			id: 6,
			name: 'DRIVE PRODUCTIVITY IN ALL CHANNELS',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/06.png',
					icons: {
						0: 'img/retailers/06/01.png',
						1: 'img/retailers/06/02.png',
						2: 'img/retailers/06/03.png',
						3: 'img/retailers/06/04.png',
						4: 'img/retailers/06/05.png',
						5: 'img/retailers/06/06.png',
					}
				}
			}
		},
		{
			id: 7,
			name: 'OWN THE VALUE CHANNEL',
			items: {
				0: {
					title: 'img/thumbnails/ourlandingretail/07.png',
					icons: {
						0: 'img/retailers/07/01.png',
						1: 'img/retailers/07/02.png',
						2: 'img/retailers/07/03.png',
						3: 'img/retailers/07/04.png',
						4: 'img/retailers/07/05.png',
						5: 'img/retailers/07/06.png',
					}
				}
			}
		}
	];

	return {
    	all: function() {
			return retailers;
    	}
  	};
})

.factory('TheMustSeeDoors', function() {
	var stores = [
		{
			id: 1,
			name: 'NSP POLANCO',
			title: 'img/thumbnails/themustseedoors/nsppolanco.jpg',
			items: {
				0: 'img/graphs/themustseedoors/nspcoyoacan/01.png',
				1: 'img/graphs/themustseedoors/nspcoyoacan/02.png',
				2: 'img/graphs/themustseedoors/nspcoyoacan/03.png',
				3: 'img/graphs/themustseedoors/nspcoyoacan/04.png',
				4: 'img/graphs/themustseedoors/nspcoyoacan/05.png',
				5: 'img/graphs/themustseedoors/nspcoyoacan/06.png',
			},
			data: 'img/data/themustseedoors/nspcoyoacan.png'
		},
		{
			id: 2,
			name: 'EL SOPE RSG',
			title: 'img/thumbnails/themustseedoors/elsopersg.png',
			items: {
				0: 'img/graphs/themustseedoors/elsopersg/01.png',
				1: 'img/graphs/themustseedoors/elsopersg/02.png',
				2: 'img/graphs/themustseedoors/elsopersg/03.png',
				3: 'img/graphs/themustseedoors/elsopersg/04.png',
				4: 'img/graphs/themustseedoors/elsopersg/05.png',
				5: 'img/graphs/themustseedoors/elsopersg/06.png',
			},
			data: 'img/data/themustseedoors/elsopersg.png'
		},
		{
			id: 3,
			name: 'LUST POLANCO',
			title: 'img/thumbnails/themustseedoors/lustpolanco.png',
			items: {
				0: 'img/graphs/themustseedoors/lustpolanco/01.png',
				1: 'img/graphs/themustseedoors/lustpolanco/02.png',
				2: 'img/graphs/themustseedoors/lustpolanco/03.png',
				3: 'img/graphs/themustseedoors/lustpolanco/04.png',
				4: 'img/graphs/themustseedoors/lustpolanco/05.png',
				5: 'img/graphs/themustseedoors/lustpolanco/06.png',
			},
			data: 'img/data/themustseedoors/lustpolanco.png'
		},
		{
			id: 4,
			name: 'INNOVASPORT + TOREO',
			title: 'img/thumbnails/themustseedoors/innovatoreo.png',
			items: {
				0: 'img/graphs/themustseedoors/innovatoreo/01.png',
				1: 'img/graphs/themustseedoors/innovatoreo/02.png',
				2: 'img/graphs/themustseedoors/innovatoreo/03.png',
				3: 'img/graphs/themustseedoors/innovatoreo/04.png',
				4: 'img/graphs/themustseedoors/innovatoreo/05.png',
				5: 'img/graphs/themustseedoors/innovatoreo/06.png',
			},
			data: 'img/data/themustseedoors/innovatoreo.png'
		},
		{
			id: 5,
			name: 'INNOVASPORT + COYOACÁN',
			title: 'img/thumbnails/themustseedoors/innovacoyoacan.png',
			items: {
				0: 'img/graphs/themustseedoors/innovacoyoacan/01.png',
				1: 'img/graphs/themustseedoors/innovacoyoacan/02.png',
				2: 'img/graphs/themustseedoors/innovacoyoacan/03.png',
				3: 'img/graphs/themustseedoors/innovacoyoacan/04.png',
				4: 'img/graphs/themustseedoors/innovacoyoacan/05.png',
				5: 'img/graphs/themustseedoors/innovacoyoacan/06.png',
			},
			data: 'img/data/themustseedoors/innovacoyoacan.png'
		},
		{
			id: 6,
			name: 'LIVERPOOL INSURGENTES',
			title: 'img/thumbnails/themustseedoors/liverpoolinsurgentes.png',
			items: {
				0: 'img/graphs/themustseedoors/liverpoolinsurgentes/01.png',
				1: 'img/graphs/themustseedoors/liverpoolinsurgentes/02.png',
				2: 'img/graphs/themustseedoors/liverpoolinsurgentes/03.png',
				3: 'img/graphs/themustseedoors/liverpoolinsurgentes/04.png',
				4: 'img/graphs/themustseedoors/liverpoolinsurgentes/05.png',
				5: 'img/graphs/themustseedoors/liverpoolinsurgentes/06.png',
			},
			data: 'img/data/themustseedoors/liverpoolinsurgentes.png'
		},
		{
			id: 7,
			name: 'INNVICTUS COYOACÁN',
			title: 'img/thumbnails/themustseedoors/innvictuscoyoacan.png',
			items: {
				0: 'img/graphs/themustseedoors/innvictuscoyoacan/01.png',
				1: 'img/graphs/themustseedoors/innvictuscoyoacan/02.png',
				2: 'img/graphs/themustseedoors/innvictuscoyoacan/03.png',
				3: 'img/graphs/themustseedoors/innvictuscoyoacan/04.png',
				4: 'img/graphs/themustseedoors/innvictuscoyoacan/05.png',
				5: 'img/graphs/themustseedoors/innvictuscoyoacan/06.png',
			},
			data: 'img/data/themustseedoors/innvictuscoyoacan.png'
		},
		{
			id: 8,
			name: 'NFS CENTRO',
			title: 'img/thumbnails/themustseedoors/nfscentro.png',
			items: {
				0: 'img/graphs/themustseedoors/nfscentro/01.png',
				1: 'img/graphs/themustseedoors/nfscentro/02.png',
				2: 'img/graphs/themustseedoors/nfscentro/03.png',
				3: 'img/graphs/themustseedoors/nfscentro/04.png',
				4: 'img/graphs/themustseedoors/nfscentro/05.png',
				5: 'img/graphs/themustseedoors/nfscentro/06.png',
			},
			data: 'img/data/themustseedoors/nfscentro.png'
		}
	];

	return {
    	all: function() {
			return stores;
    	}
  	};
})

.factory('Agenda', function() {
	var events = [
		{
			id: 1,
			date: 'MON. 14 DEC.',
			items: {
				0: {
					'time': '8:OO - 13:00',
					'name': 'Mexico Presentation',
					'participants': 'EMLT / MX LT',
					'place': 'ST REGIS HOTEL'
				},
				1: {
					'time': '13:00 - 14:00',
					'name': 'Lunch @ ST. Regis Terrace',
					'participants': 'EMLT / MX LT',
					'place': 'ST REGIS HOTEL'
				},
				2: {
					'time': '14:00 - 14:15',
					'name': 'Refresh',
					'participants': 'EMLT / MX LT',
					'place': 'ST REGIS HOTEL'
				},
				3: {
					'time': '14:00 - 19:00',
					'name': 'Mexico City Market place',
					'participants': 'EMLT / MX LT',
					'place': '<div style="margin-top:-54px;"><span class="rem80 uppercase block">COYOACAN</span><span class="rem80 uppercase block">INNOVASPOTS +</span><span class="rem80 uppercase block">NPS, INNVICTUS</span><span class="rem80 uppercase block">LIVERPOOL INSURGENTES</span><span class="rem80 uppercase block">REFORMA 222</span><span class="rem80 uppercase lock">INNOVASPORT POLANCO TBD</span></div>'
				},
				4: {
					'time': '19:00 - 21:00',
					'name': 'Consumer Experience',
					'participants': 'EMLT / MX LT',
					'place': 'HOUSE OF HER'
				},
				5: {
					'time': '21:00 - 23:00',
					'name': 'Dinner',
					'participants': 'EMLT / MX LT',
					'place': '<div style="margin-top: -12px;">99 Nueve Nueve<br/>Restaurant</div>'
				},
				6: {
					'time': '23:00',
					'name': 'Transport to Hotel',
					'participants': 'EMLT / MX LT',
					'place': 'ST REGIS HOTEL'
				}
			}
		},
		{
			id: 1,
			date: 'TUE. 15 DEC.',
			items: {
				0: {
					'time': '9:00',
					'name': 'Mx Depart to Guadalajara',
					'participants': 'MX LT',
					'place': 'ST REGIS HOTEL'
				},
				1: {
					'time': 'TBD',
					'name': 'Brazil & Soco Market visit',
					'participants': 'TBD',
					'place': 'MEXICO CITY'
				}
			}
		}
	];

	return {
    	all: function() {
			return events;
    	},
    	get: function(eventId) {
			for (var i = 0; i < events.length; i++) {
				if (events[i].id === parseInt(eventId)) {
					return events[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('TheTourOfToday', function() {
	var courses = [
		{
			id: 1,
			name: 'MONDAY',
			day: 'img/14th.png',
			items: {
				0: {
					'id': '1',
					'name': 'NSP Oasis Coyoacan',
				},
				1: {
					'id': '2',
					'name': 'Innovasport + Oasis Coyoacan',
				},
				2: {
					'id': '3',
					'name': 'Innvictus Oasis Coyoacan'
				},
				3: {
					'id': '4',
					'name': 'Liverpoool Insurgentes'
				},
				4: {
					'id': '5',
					'name': 'NSP Reforma 222'
				},
				5: {
					'id': '6',
					'name': 'Innvictus Reforma 222'
				},
				6: {
					'id': '7',
					'name': 'HOUSE OF HER'
				}
			}
		}
	];

	return {
    	all: function() {
			return courses;
    	},
		get: function(courseId) {
			for (var i = 0; i < courses.length; i++) {
				if (courses[i].id === parseInt(courseId)) {
					return courses[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('TheCourseOfToday', function() {
	var courses = [
		{
			id: 1,
			name: 'NSP Oasis Coyoacan',
			cover: 'img/covers/thecourseoftoday/nspoasiscoyoacan.jpg',
			title: 'img/titles/thecourseoftoday/nspoasiscoyoacan.jpg',
			graph: 'img/graphs/thecourseoftoday/nspoasiscoyoacan.png'
		},
		{
			id: 2,
			name: 'Innovasport + Oasis Coyoacan',
			cover: 'img/covers/thecourseoftoday/innovacoyoacan.jpg',
			title: 'img/titles/thecourseoftoday/innovacoyoacan.jpg',
			graph: 'img/graphs/thecourseoftoday/innovacoyoacan.png'
		},
		{
			id: 3,
			name: 'Innvictus Oasis Coyoacan',
			cover: 'img/covers/thecourseoftoday/innvictuscoyoacan.jpg',
			title: 'img/titles/thecourseoftoday/innvictuscoyoacan.jpg',
			graph: 'img/graphs/thecourseoftoday/innvictuscoyoacan.png'
		},
		{
			id: 4,
			name: 'Liverpoool Insurgentes',
			cover: 'img/covers/thecourseoftoday/liverpoolinsurgentes.jpg',
			title: 'img/titles/thecourseoftoday/liverpoolinsurgentes.jpg',
			graph: 'img/graphs/thecourseoftoday/liverpoolinsurgentes.png'
		},
		{
			id: 5,
			name: 'NSP Reforma 222',
			cover: 'img/covers/thecourseoftoday/nspreforma.jpg',
			title: 'img/titles/thecourseoftoday/nspreforma.jpg',
			graph: 'img/graphs/thecourseoftoday/nspreforma.png'
		},
		{
			id: 6,
			name: 'Innvictus Reforma 222',
			cover: 'img/covers/thecourseoftoday/innvictusreforma.jpg',
			title: 'img/titles/thecourseoftoday/innvictusreforma.jpg',
			graph: 'img/graphs/thecourseoftoday/innvictusreforma.png'
		},
		{
			id: 7,
			name: 'HOUSE OF HER',
			cover: 'img/covers/thecourseoftoday/houseofher.jpg',
			title: 'img/titles/houseofher.jpg',
			thumbnail: 'img/graphs/thecourseoftoday/houseofher/thumbnail.jpg',
			content: '<p>Nike Women Mexico space offers top services to engage and motivate Nike+ members to become the best athletes they can be. Starting with gait analysis and bra fittings, members accelerate their training through NTC and NRC live experiences. Five floors of Nike’s best services and product innovations. In addition to state-of-the art studio spaces and equipment. Members can shop the newest Nike collections. A Sneakers Bar gives members the opportunity to bring in their favorite Nike footwear for a ‘tune up,’ or cleaning. WIFI throughout the house allows members to share and celebrate each other’s achievements via the Nike+ Apps. Additionally, guests can print their Instagram photos and leave them in the house to further inspire and motivate others. After hours of fun and intense training, members can relax and hang out on the cozy terrace, have a massage and get hydrated at the juice bar.<p>',
			numbers: 'img/graphs/thecourseoftoday/houseofher/numbers.png',
			items: {
				0: 'img/graphs/thecourseoftoday/houseofher/01.png',
				1: 'img/graphs/thecourseoftoday/houseofher/02.png',
				2: 'img/graphs/thecourseoftoday/houseofher/03.png',
			}
		},
	];

	return {
    	all: function() {
			return courses;
    	},
		get: function(courseId) {
			for (var i = 0; i < courses.length; i++) {
				if (courses[i].id === parseInt(courseId)) {
					return courses[i];
        		}
      		}
	  		return null;
    	}
  	};
})

.factory('Chingopedia', function() {
	var list = [
		{
			id: 1,
			name: 'A',
			icon: 'img/chingopedia/a.png',
			items: {
				0: {
					'word': 'Ahorita',
					'definition': 'Now, but probably never.'
				},
				1: {
					'word': 'Ahuevo',
					'definition': 'Definitely yes, indication of victory.'
				},
				2: {
					'word': 'Ajalas',
					'definition': 'I agree'
				},
				3: {
					'word': 'Ándale',
					'definition': 'Hurry up”, “Come on”; Affirmation; “You are right”.'
				},
				4: {
					'word': 'Antro',
					'definition': 'The night club.'
				},
				5: {
					'word': 'Ardido',
					'definition': 'A person so mad and frustrated about something that you can tell rigth away.'
				},
				6: {
					'word': 'Arre',
					'definition': 'Positive, affirmation.'
				},
				7: {
					'word': 'Aventón',
					'definition': 'Hitchhike or free ride.'
				}
			}
		},
		{
			id: 2,
			name: 'C',
			icon: 'img/chingopedia/c.png',
			items: {
				0: {
					'word': 'Cachondo',
					'definition': 'Kinky or horny.'
				},
				1: {
					'word': 'Camara',
					'definition': 'Goodbye or see you later.'
				},
				2: {
					'word': 'Carnal',
					'definition': 'Buddy, friend, brother.'
				},
				3: {
					'word': 'Cascara',
					'definition': 'Informal football match.'
				},
				4: {
					'word': 'Chachara',
					'definition': 'Bargain, low value thing.'
				},
				5: {
					'word': 'Chale',
					'definition': 'Indicates disagreement.'
				},
				6: {
					'word': 'Chamba',
					'definition': 'Job. Chambear (to work).'
				},
				7: {
					'word': 'Chance',
					'definition': 'A possibility taken into consideration, maybe.'
				},
				8: {
					'word': 'Chela',
					'definition': 'Short term for “cerveza”, “beer”.'
				},
				9: {
					'word': 'Chido',
					'definition': 'Something cool.'
				},
				10: {
					'word': 'Chilango',
					'definition': 'Mexicans born in the Federal district used by mexicans from the rest of the country.'
				},
				11: {
					'word': 'Chinga',
					'definition': 'Substitute verb of word for any word that you dont know in spanish.'
				},
				12: {
					'word': 'Chingada',
					'definition': 'A very, very, I MEAN VERY far away place; Screwed or damned.'
				},
				13: {
					'word': 'Chingón',
					'definition': 'Something really really cool or awesome.'
				},
				14: {
					'word': 'Chula',
					'definition': 'Cute or good looking mexican girl.'
				},
				15: {
					'word': 'Codo',
					'definition': 'Being stingy about money.'
				},
				16: {
					'word': 'Compa',
					'definition': 'A real close friend.'
				},
				17: {
					'word': 'Cotorreo',
					'definition': 'A fun conversation with somebody.'
				},
				18: {
					'word': 'Cruda',
					'definition': 'Too many tequilas last night.'
				},
				19: {
					'word': 'Cuate',
					'definition': 'Friend or brother.'
				}
			}
		},
		{
			id: 3,
			name: 'D',
			icon: 'img/chingopedia/d.png',
			items: {
				0: {
					'word': 'Dar el rol',
					'definition': 'To hang out and party.'
				},
				1: {
					'word': 'De volada',
					'definition': 'Very fast action.'
				}
			}
		},
		{
			id: 4,
			name: 'E',
			icon: 'img/chingopedia/e.png',
			items: {
				0: {
					'word': 'Esta cura',
					'definition': 'Something funny.'
				}
			}
		},
		{
			id: 5,
			name: 'F',
			icon: 'img/chingopedia/f.png',
			items: {
				0: {
					'word': 'Fregón',
					'definition': 'Awesome or Kick ass.'
				}
			}
		},
		{
			id: 6,
			name: 'G',
			icon: 'img/chingopedia/g.png',
			items: {
				0: {
					'word': 'Gacho',
					'definition': 'Bad quality, to be mean.'
				},
				1: {
					'word': 'Güey',
					'definition': 'Mexican way of saying “dude”, It´s pronounced “way”'
				}
			}
		},
		{
			id: 7,
			name: 'H',
			icon: 'img/chingopedia/h.png',
			items: {
				0: {
					'word': 'Harto',
					'definition': 'A lot of something.'
				},
				1: {
					'word': 'Hijoles',
					'definition': 'Wow!.'
				},
				2: {
					'word': 'Hocho',
					'definition': 'Hot dog.'
				}
			}
		},
		{
			id: 8,
			name: 'I',
			icon: 'img/chingopedia/i.png',
			items: {
				0: {
					'word': 'Ira nomás',
					'definition': 'Finding something out in a surprised way “look at that”'
				}
			}
		},
		{
			id: 9,
			name: 'J',
			icon: 'img/chingopedia/j.png',
			items: {
				0: {
					'word': 'Jefa',
					'definition': 'Mom.'
				}
			}
		},
		{
			id: 10,
			name: 'K',
			icon: 'img/chingopedia/k.png',
			items: {
				0: {
					'word': 'Keka',
					'definition': 'Quesadilla.'
				}
			}
		},
		{
			id: 11,
			name: 'L',
			icon: 'img/chingopedia/l.png',
			items: {
				0: {
					'word': 'La banda',
					'definition': 'A group of friends “the gang” or “the guys”.'
				},
				1: {
					'word': 'Ligar',
					'definition': 'To “hook up”.'
				}
			}
		},
		{
			id: 12,
			name: 'M',
			icon: 'img/chingopedia/m.png',
			items: {
				0: {
					"word": "Machin",
					"definition": "Disminutive for “macho” used to call a friend. Something cool."
				},
				1: {
					"word": "Mamacita",
					"definition": "Really sexy mexican girl."
				},
				2: {
					"word": "Me atoraron",
					"definition": "I got screwed."
				},
				3: {
					"word": "Me la hizo de tos",
					"definition": "Someone complicated my existence, looking for trouble."
				},
				4: {
					"word": "Me la pelan",
					"definition": "I´m the best here."
				},
				5: {
					"word": "Mezcalito",
					"definition": "Tequila´s close cousin famous for the trademark worm inside the bottle."
				},
				6: {
					"word": "Mija (o)",
					"definition": "Refers to “my little son” talking to a boy / girl."
				},
				7: {
					"word": "Mirrey",
					"definition": "Spoiled rich kid who likes to squander money. Seeking for attention with their overrated “good taste”."
				},
				8: {
					"word": "Mitotero",
					"definition": "Noisey guy that likes to gossip around."
				},
				9: {
					"word": "Morra",
					"definition": "Informal way of referring to a young girl."
				},
				10: {
					"word": "Morro",
					"definition": "Informal way of referring to little kid."
				},
				11: {
					"word": "Muy perro",
					"definition": "Something very cool."
				}
			}
		},
		{
			id: 13,
			name: 'N',
			icon: 'img/chingopedia/n.png',
			items: {
				0: {
					"word": "Naco",
					"definition": "Really bad taste for style or fashion."
				},
				1: {
					"word": "Naik",
					"definition": "The real name of “Nike” here in Mexico badly pronounced."
				},
				2: {
					"word": "Naranjas",
					"definition": "Negative response."
				},
				3: {
					"word": "Narco",
					"definition": "Common term for a “drug dealer” or drug culture in Mexico."
				},
				4: {
					"word": "Nel",
					"definition": "Negative response."
				},
				5: {
					"word": "Neta",
					"definition": "For real."
				},
				6: {
					"word": "Nini",
					"definition": "Young person who doesn´t work or study."
				},
				7: {
					"word": "No mames",
					"definition": "“You´re kidding” or “no way”"
				},
				8: {
					"word": "Ñora",
					"definition": "Old woman mid “40´s”"
				}
			}
		},
		{
			id: 14,
			name: 'O',
			icon: 'img/chingopedia/o.png',
			items: {
				0: {
					'word': 'Orale',
					'definition': 'Surprise or understanding.'
				}
			}
		},
		{
			id: 15,
			name: 'P',
			icon: 'img/chingopedia/p.png',
			items: {
				0: {
					"word": "Pedo",
					"definition": "Fart or drunk."
				},
				1: {
					"word": "Pendejo",
					"definition": "An asshole in any possible way."
				},
				2: {
					"word": "Pinche",
					"definition": "It´s an all-porpouse insult enhancer, equivalent to the use of “f*ck” in english"
				},
				3: {
					"word": "Poca madre",
					"definition": "Something awesome."
				},
				4: {
					"word": "Pomo",
					"definition": "A bottle of alcoholic beverage."
				},
				5: {
					"word": "Puto",
					"definition": "Very offensive way to call cowards. rival´s team goalie."
				}
			}
		},
		{
			id: 16,
			name: 'Q',
			icon: 'img/chingopedia/q.png',
			items: {
				0: {
					"word": "Que ondas",
					"definition": "Friendly way of saying “what´s up”."
				},
				1: {
					"word": "Que padre",
					"definition": "Thats cool."
				},
				2: {
					"word": "Que pex",
					"definition": "Friendly way of saying “whats up”",
				},
				3: {
					"word": "Que tranza",
					"definition": "Friendly way of saying “whats up”"
				}
			}
		},
		{
			id: 17,
			name: 'R',
			icon: 'img/chingopedia/r.png',
			items: {
				0: {
					"word": "Radio pasillo",
					"definition": "The fastest way of spreading news in a company by gossiping."
				},
				1: {
					"word": "Rola",
					"definition": "Common way to call a song."
				},
				2: {
					"word": "Romper madre",
					"definition": "To “Kick someone ass”."
				}
			}
		},
		{
			id: 18,
			name: 'S',
			icon: 'img/chingopedia/s.png',
			items: {
				0: {
					"word": "Saluchita",
					"definition": "Cheers."
				},
				1: {
					"word": "Simon",
					"definition": "All porpouse affirmation."
				}
			}
		},
		{
			id: 19,
			name: 'T',
			icon: 'img/chingopedia/t.png',
			items: {
				0: {
					"word": "Te cae?",
					"definition": "Do you think so?"
				},
				1: {
					"word": "Tequila",
					"definition": "Mexican juice of gods that replenishes the soul."
				},
				2: {
					"word": "Tocho morocho",
					"definition": "A little of everything."
				},
				3: {
					"word": "Tostón",
					"definition": "50 cents from a mexican “peso”."
				}
			}
		},
		{
			id: 20,
			name: 'V',
			icon: 'img/chingopedia/v.png',
			items: {
				0: {
					"word": "Vato",
					"definition": "Mexican slang for “dude” or “man”"
				},
				1: {
					"word": "Vientos!",
					"definition": "Informal way of expressing the word “good”"
				}
			}
		},
		{
			id: 21,
			name: 'Y',
			icon: 'img/chingopedia/y.png',
			items: {
				0: {
					"word": "Ya me cargo el payaso",
					"definition": "Damn, I’m crewed."
				},
				1: {
					"word": "Ya mero",
					"definition": "Almost there."
				},
				2: {
					"word": "Yo me lanzo",
					"definition": "I go."
				},
				3: {
					"word": "Yo te picho",
					"definition": "Paying for someone else tab, “i got this”"
				}
			}
		}
	];

	return {
    	all: function() {
			return list;
    	}
  	};
})

.factory('API',['$http','PARSE_CREDENTIALS',function($http,PARSE_CREDENTIALS){

	var mydata = {};

	return {
		doLogin:function(email,password) {
			var config = {
            	headers: {
					'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
					'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY
				},
				params: {
                	username: email,
					password: password
				}
            }
			return $http.get('https://api.parse.com/1/login', config);
        },
        doLogout:function(session) {
			var config = {
            	headers: {
					'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
					'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
					'X-Parse-Session-Token': session
				}
            }
			return $http.post('https://api.parse.com/1/logout', mydata, config);
        },
        createUser: function(username, email, password, gender, country)
        {
	        var config = {
            	headers: {
					'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
					'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
					'X-Parse-Revocable-Session': 1,
					'Content-Type': 'application/json'
				},
				params: {
                	username: email,
					password: password,
					nickname: username,
					name: "",
					allowUser: false,
					email: email
				}
            }
			return $http.post('https://api.parse.com/1/users', mydata, config);
        },
        getToursAll:function() {
			return $http.get('https://api.parse.com/1/classes/tour',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
                },
				params: {
					order: '-createdAt'
				}
            });
        },
        getPhotofeedAll:function() {
			return $http.get('https://api.parse.com/1/classes/photofeed',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
                },
				params: {
					order: '-createdAt'
				}
            });
        },
        getPhotofeedUser:function(ownerID) {
	        return $http.get('https://api.parse.com/1/classes/photofeed',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
                },
				params: {
					order: '-createdAt',
                	where: {"owner": {"__type":"Pointer","className":"_User","objectId":ownerID}}
				}
            });
        },
		getTeam:function() {
			return $http.get('https://api.parse.com/1/classes/team',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
                },
                params: {
					limit: '1000'
				}
            });
        },
        getTeamMember:function() {
			return $http.get('https://api.parse.com/1/classes/team',{
                headers:{
                    'X-Parse-Application-Id': PARSE_CREDENTIALS.APP_ID,
                    'X-Parse-REST-API-Key': PARSE_CREDENTIALS.REST_API_KEY,
                }
            });
        }
	}

}])

.value('PARSE_CREDENTIALS',{
    APP_ID: 'yIkjB91C7MHUk4lynlMpOraUnsebIZWjCqiF1XxW',
    REST_API_KEY:'RDTL4E0GEkgND14modyLqNc2x6kGJMvM1sJxLam0'
});